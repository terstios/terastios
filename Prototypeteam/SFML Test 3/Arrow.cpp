#include "stdafx.h"
#include "Arrow.h"


Arrow::Arrow()
{
	m_bVisible = true;
	rect.setSize(sf::Vector2f(14, 24));
	rect.setFillColor(sf::Color::White);
}

Arrow::~Arrow()
{
}

bool Arrow::CheckHit()
{
	if (m_bVisible == false)
		return false;
	else
		return true;
}

void Arrow::SetVisible()
{
	m_bVisible = false;
}

void Arrow::update()
{
	if (Deathcounter == 0)
	{
		Erase = true;
	}
	Deathcounter--;
	if (AnimationCounter == 4)
	{
		AnimationsX++;
		AnimationCounter = 0;
	}
	AnimationCounter++;

	if (AnimationsX * 32 >= 128)
	{
		AnimationsX = 0;
	}

	sprite.setTextureRect(sf::IntRect(AnimationsX * 32, AnimationsY, 32, 48));
	
	sprite.setPosition(sf::Vector2f(ArrowX += cosine * 15, ArrowY += sine * 15));
	
	rect.setPosition(sprite.getPosition().x,sprite.getPosition().y);
}

void Arrow::fire(float p_fMousePosY, float p_fMousePosX, float p_fPlayerPosY, float p_fPlayerPosX)
{
	
	Angle = atan2(p_fMousePosY - p_fPlayerPosY - 32,
		p_fMousePosX - p_fPlayerPosX - 32);

	Angle * 180 / PI;
	cosine = cos(Angle);// Note: This angle is in radians.
	sine = sin(Angle);

	ArrowX = p_fPlayerPosX + 32;
	ArrowY = p_fPlayerPosY + 32;

}

void Arrow::SetSpeedBoost(float p_fSpeed)
{
	SpeedBoost = p_fSpeed;
}
