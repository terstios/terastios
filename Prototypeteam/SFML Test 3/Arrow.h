#pragma once
#include "iEntity.h"

class Arrow : public iEntity
{
public:
	Arrow();
	~Arrow();
	
	bool CheckHit();
	void SetVisible();
	void update();
	void fire(float p_fMousePosY, float p_fMousePosX, float p_fPlayerPosY, float p_fPlayerPosX);
	void SetSpeedBoost(float p_fSpeed);
	bool Erase = false;
private:
	int Deathcounter = 600;
	float SpeedBoost = 0;
	float PI = 3.14159265;
	float PlayerX = 0;
	float PlayerY = 0;
	float ArrowX = 0;
	float ArrowY = 0;
	float cosine = 0;
	float sine = 0;
	float Angle = 0;
	bool m_bVisible;
	int AnimationCounter = 0;
	int AnimationsX = 0;
	int AnimationsY = 0;

};

