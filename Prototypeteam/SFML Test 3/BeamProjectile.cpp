#include "stdafx.h"
#include "BeamProjectile.h"



BeamProjectile::BeamProjectile()
{
	rect.setSize(sf::Vector2f(32 * sprite.getScale().x, 1024 * sprite.getScale().y));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(EnemyAnimationsX * 32, EnemyAnimationsY * 1024, 32, 1024));
}


BeamProjectile::~BeamProjectile()
{
}

void BeamProjectile::update()
{
	if (Deathcounter == 0)
	{
		Erase = true;
	}
	Deathcounter--;

	if (AnimationCounter == 4)
	{
		EnemyAnimationsX++;
		AnimationCounter = 0;

	}
	AnimationCounter++;

	if (EnemyAnimationsX == 10)
	{
		m_bVisible = false;
	}
	
	m_fY += 3 + SpeedPower;
	
	sprite.setPosition(m_fX, m_fY);
	rect.setPosition(sprite.getPosition().x, sprite.getPosition().y);
	sprite.setTextureRect(sf::IntRect(EnemyAnimationsX * 32, EnemyAnimationsY * 1024, 32, 1024));
}

void BeamProjectile::SetBeamerPos(float p_fBeamerEnemyPosX, float p_fBeamerEnemyPosY)
{
	m_fX = p_fBeamerEnemyPosX;
	m_fY = p_fBeamerEnemyPosY;
}

void BeamProjectile::SetVisible()
{
	m_bVisible = false;
}

bool BeamProjectile::CheckHit()
{
	if (!m_bVisible)
		return false;
	else
		return true;
}

void BeamProjectile::SetSpeedBoost(float speed)
{
	SpeedPower = speed;
}
