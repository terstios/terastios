#pragma once
#include "iEntity.h"

class BeamProjectile : public iEntity
{
public:
	BeamProjectile();
	~BeamProjectile();

	void update();
	void SetBeamerPos(float p_fBeamerEnemyPosX, float p_fBeamerEnemyPosY);
	void SetVisible();
	bool CheckHit();
	void SetSpeedBoost(float speed);

	bool Erase = false;
private:
	int Deathcounter = 600;
	float SpeedPower = 0;
	bool m_bVisible = true;
	float PI = 3.14159265;
	float m_fX = 0;
	float m_fY = 0;
	float PlayerX = 0;
	float PlayerY = 0;
	float EarthBossProjectileX;
	float EarthBossProjectileY;
	float cosine = 0;
	float sine = 0;
	float Angle = 0;
	int AnimationCounter = 0;
	int EnemyAnimationsX = 0;
	int EnemyAnimationsY = 2;
};

