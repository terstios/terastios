#include "stdafx.h"
#include "BeamerEnemy.h"



BeamerEnemy::BeamerEnemy()
{
	sprite.setScale(2, 2);
	m_bVisible = true;
	rect.setSize(sf::Vector2f(24 * sprite.getScale().x, 32 * sprite.getScale().y));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(EnemyAnimationsX * 64, EnemyAnimationsY * 64, 64, 64));
}


BeamerEnemy::~BeamerEnemy()
{
}

void BeamerEnemy::update()
{
	if (Deathcounter == 0)
	{
		Erase = true;
	}
	Deathcounter--;
	if (AnimationCounter == 4)
	{
		EnemyAnimationsX++;
		AnimationCounter = 0;

	}
	AnimationCounter++;
	if (m_bDying)
	{
		m_fY += 5;

		if (EnemyAnimationsX * 64 >= 64 * Size)
		{
			EnemyAnimationsX = 0;
			if (EnemyAnimationsY == 4)
			{
				m_bVisible = false;
			}
			EnemyAnimationsY++;
		}
	}
	else
	{
		if (ChilloutTimer >= 60)
		{
			if (!ActiveAttack)
			{
				int Attacking = randomBeamerChecker.ReturnRandom1to3();
				if (Attacking >= 0)
				{
					Size = 15;
					EnemyAnimationsX = 0;
					EnemyAnimationsY = 0;
					ActiveAttack = true;
					Attack = true;
				}
			}



			if (Attack)
			{
				if (EnemyAnimationsX * 64 >= 64 * Size)
				{
					Size = 11;
					EnemyAnimationsX = 0;
					EnemyAnimationsY = 2;
					ChilloutTimer = 0;
					ActiveAttack = false;
					Attack = false;
					m_bFire = false;
				}
			}

		}
		else
		{

			if (EnemyAnimationsX * 64 >= 64 * Size)
			{
				EnemyAnimationsX = 0;
			}
			ChilloutTimer++;
		}
		if (AttackDelay >= 4)
		{
			if (EnemyAnimationsX == 12 && EnemyAnimationsY == 0)
			{
				m_bFire = true;
				AttackDelay = 0;
			}
		}
		AttackDelay++;
		m_fY += 3 + SpeedBoost;
	}
	

	

	sprite.setPosition(m_fX, m_fY);
	rect.setPosition(sprite.getPosition().x + rect.getSize().x / 2 + 14, sprite.getPosition().y + rect.getSize().y / 2);
	
	if (m_fY > 1024)
		m_fY = -64;
	sprite.setTextureRect(sf::IntRect(EnemyAnimationsX * 64, EnemyAnimationsY * 64, 64, 64));
}

bool BeamerEnemy::CheckHit()
{
	if (!m_bVisible)
		return false;
	else
		return true;
}

void BeamerEnemy::SetVisible()
{
	EnemyAnimationsX = 0;
	EnemyAnimationsY = 3;
	Size = 10;
	m_bDying = true;
}

void BeamerEnemy::SetSpeedBoost(float p_fSpeed)
{
	SpeedBoost = p_fSpeed;
}

void BeamerEnemy::SetPos(float NewPosX, float NewPosY)
{
	m_fX = NewPosX;
	m_fY = NewPosY;
}

bool BeamerEnemy::CheckFire()
{
	if (m_bFire)
	{
		m_bFire = false;
		return true;
	}
		
	else
		return false;
}


