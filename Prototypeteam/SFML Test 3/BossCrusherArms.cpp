#include "stdafx.h"
#include "BossCrusherArms.h"



BossCrusherArms::BossCrusherArms()
{
	sprite.setScale(6, 6);
	m_bVisible = true;
	rect.setSize(sf::Vector2f(32 * sprite.getScale().x, 24 * sprite.getScale().y));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(AnimationsX * 96, AnimationsY * 64, 96, 64));
}


BossCrusherArms::~BossCrusherArms()
{
}

void BossCrusherArms::update()
{
	if (Deathcounter == 0)
	{
		Erase = true;
	}
	Deathcounter--;
	if (Crush)
	{
		if (Waittimer >= 30)
		{
			if (AnimationsY == 0 || AnimationsY == 1)
			{
				if (m_fX <= 452)
				{
					AnimationsY = 1;
					Retract = true;
					Crush = false;
				}
				else
				m_fX -= 64;
			}

			if (AnimationsY == 2 || AnimationsY == 3)
			{
				if (m_fX >= 578 - 96 * sprite.getScale().x)
				{
					AnimationsY = 3;
					Retract = true;
					Crush = false;
				}
				else
				m_fX += 64;
			}
		}
		Waittimer++;

		
	}
	else if (Retract)
	{
		if (Waittimer2 >= 15)
		{
			if (AnimationsY == 1 || AnimationsY == 0)
			{
				m_fX += 16;
			}

			if (AnimationsY == 2 || AnimationsY == 3)
			{
				m_fX -= 16;
			}

		}
		Waittimer2++;
	}
	else
	{
		if (AnimationsY == 0)
		{
			m_fX -= 5;
			if (m_fX <= 896)
				Crush = true;
		}

		if (AnimationsY == 2)
		{
			m_fX += 5;
			if (m_fX >= 128 - 96 * sprite.getScale().x)
				Crush = true;
		}
	}
	if (AnimationsY == 0 || AnimationsY == 1)
	{
		rect.setPosition(m_fX + 32, m_fY - 8 * sprite.getScale().y - 20);
	}

	if (AnimationsY == 2 || AnimationsY == 3)
	{
		rect.setPosition(m_fX + 52 * sprite.getScale().x , m_fY - 8 * sprite.getScale().y - 20);
	}
	
	sprite.setTextureRect(sf::IntRect(AnimationsX * 96, AnimationsY * 64, 96, 64));
	sprite.setPosition(m_fX, m_fY - 32 * sprite.getScale().y - 64);
}

void BossCrusherArms::Shoot(float p_fPlayerPosX, float p_fPlayerPosY, float arm)
{
	m_fX = p_fPlayerPosX;
	m_fY = p_fPlayerPosY;
	AnimationsY = arm;
}


