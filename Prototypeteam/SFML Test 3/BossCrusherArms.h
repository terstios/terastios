#pragma once
#include "iEntity.h"

class BossCrusherArms : public iEntity
{
public:
	BossCrusherArms();
	~BossCrusherArms();

	void update();
	void Shoot(float p_fPlayerPosX, float p_fPlayerPosY, float arm);
	 
	bool Erase = false;
private:
	int Deathcounter = 600;
	bool Retract = false;
	int Waittimer = 0;
	int Waittimer2 = 0;
	bool Crush = false;
	bool m_bHitboxactive = false;
	float m_fX = 0;
	float m_fY = 0;
	bool m_bVisible;
	int AnimationsX = 0;
	int AnimationsY = 2;
	int AnimationCounter = 0;
	int Size = 4;
};

