#include "stdafx.h"
#include "EarthBossEnemy.h"




EarthBossEnemy::EarthBossEnemy()
{
	sprite.setScale(2.5, 2.5);
	rect.setSize(sf::Vector2f(48 * sprite.getScale().x, 48 * sprite.getScale().y));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(EnemyAnimationsX * 128, EnemyAnimationsY * 128, 128, 128));
	Originalcolor = sprite.getColor();
	color = sprite.getColor();
	
}


EarthBossEnemy::~EarthBossEnemy()
{
}

void EarthBossEnemy::update()
{
	if (!m_bVisible)
	{

	}
	else
	{

		if (AnimationCounter == 4)
		{
			EnemyAnimationsX++;
			AnimationCounter = 0;

		}
		AnimationCounter++;

		if (ChilloutTimer >= 60)
		{
			if (!ActiveAttack)
			{
				int WhatAttack = randomBossChecker.ReturnRandom1to3();
				if (WhatAttack == 1)
				{
					Size = 5;
					attack1 = true;
					ActiveAttack = true;
					EnemyAnimationsX = 0;
					EnemyAnimationsY = 0;
				}
				else if (WhatAttack == 2)
				{
					Size = 9;
					attack2 = true;
					ActiveAttack = true;
					EnemyAnimationsX = 5;
					EnemyAnimationsY = 0;
				}
				else if (WhatAttack == 3)
				{
					Size = 13;
					attack3 = true;
					ActiveAttack = true;
					EnemyAnimationsX = 10;
					EnemyAnimationsY = 0;
				}
			}
			
			if (attack1)
			{
				if (EnemyAnimationsX * 128 >= 128 * Size)
				{
					EnemyAnimationsX = 0;
					if (EnemyAnimationsY == 0)
						EnemyAnimationsY++;
					else if (EnemyAnimationsY == 1)
						EnemyAnimationsY++;
					else if (EnemyAnimationsY == 2)
					{
						Size = 4;
						EnemyAnimationsY++;
					}
					else if (EnemyAnimationsY == 3)
					{
						EnemyAnimationsY++;
					}
					else if (EnemyAnimationsY == 4)
					{
						Size = 8;
						EnemyAnimationsX = 5;
						EnemyAnimationsY = 6;
						attack1 = false;
						ChilloutTimer = 0;
						ActiveAttack = false;
					}
				}
			}
			else if (attack2)
			{
				if (EnemyAnimationsX * 128 >= 128 * Size)
				{
					EnemyAnimationsX = 5;
					EnemyAnimationsY++;
					if (EnemyAnimationsY == 3)
						Size = 8;
					if (EnemyAnimationsY == 4)
					{
						EnemyAnimationsX = 5;
						EnemyAnimationsY = 6;
						attack2 = false;
						ChilloutTimer = 0;
						ActiveAttack = false;
					}

				}
			}
			else if (attack3)
			{
				if (EnemyAnimationsX * 128 >= 128 * Size)
				{
					EnemyAnimationsX = 10;
					EnemyAnimationsY++;
					if (EnemyAnimationsY == 3)
					{
						attack3 = false;
						
						ChilloutTimer = 0;
						EnemyAnimationsX = 5;
						EnemyAnimationsY = 6;
						ActiveAttack = false;
					}
				}
			}
		}
		
		else
		{
			Size = 8;

			if (EnemyAnimationsX * 128 >= 128 * Size)
			{
				EnemyAnimationsX = 5;
			}

			ChilloutTimer++;
		}
		

		if (EnemyAnimationsX == 7 && EnemyAnimationsY == 3)
		{
			m_bImpale = true;
		}

		if (EnemyAnimationsX == 3 && EnemyAnimationsY == 2)
		{
			m_bFire = true;
		}

		if (EnemyAnimationsX == 12 && EnemyAnimationsY == 2)
		{
			m_bCrush = true;
		}

		/*if (EnemyAnimationsX * 128 >= 128 * Size)
		{
			EnemyAnimationsX = 10;
		}*/
		if (m_bDamaged == true)
		{
			if (counter == 15)
			{
				color.b = 100;
				color.g = 100;
			}
			else if (counter == 10)
			{
				color.b = 0;
				color.g = 0;
			}
			else if (counter == 5)
			{

				color.b = 100;
				color.g = 100;
			}
			else
			{
				color.b = 255;
				color.g = 255;
				color.r = 255;
			}
			counter++;

			sprite.setColor(color);
		}

		sprite.setPosition(m_fX, m_fY);
		rect.setPosition(sprite.getPosition().x + 40 * sprite.getScale().x, sprite.getPosition().y + 16 * sprite.getScale().y);

		sprite.setTextureRect(sf::IntRect(EnemyAnimationsX * 128, EnemyAnimationsY * 128, 128, 128));
	}
}

bool EarthBossEnemy::CheckHit()
{
	if (m_bVisible == false)
		return false;
	else
		return true;
}

void EarthBossEnemy::SetVisible()
{
	if (m_bDamaged)
	{
		m_iHealth--;
	}
	m_bDamaged = true;
	counter = 0;
}

bool EarthBossEnemy::ReadytoFire()
{
	if (m_bFire)
	{
		m_bFire = false;
		return true;
	}
		
	else
	{
		return false;
	}
	
}

bool EarthBossEnemy::ReadytoImpale()
{
	if (m_bImpale)
	{
		m_bImpale = false;
		return true;
	}

	else
	{
		return false;
	}

}

bool EarthBossEnemy::ReadytoCrush()
{
	if (m_bCrush)
	{
		m_bCrush = false;
		return true;
	}
	else
	{
		return false;
	}
}
