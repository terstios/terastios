#pragma once
#include "iEntity.h"
#include "RandomReturn.h"

class EarthBossEnemy : public iEntity
{
public:
	EarthBossEnemy();
	~EarthBossEnemy();

	void update();
	bool CheckHit();
	void SetVisible();
	bool ReadytoFire();
	bool ReadytoImpale();
	bool ReadytoCrush();
	
	sf::Color color;
	sf::Color Originalcolor;
	class RandomReturn randomBossChecker;
private:
	bool attack1 = false;
	bool attack2 = false;
	bool attack3 = false;
	int ChilloutTimer = 0;
	bool ActiveAttack = false;
	int counter = 0;
	bool m_bDamaged = false;
	bool m_bCrush = false;
	bool m_bImpale = false;
	bool m_bVisible = true;
	bool m_bFire = false;
	int m_iHealth = 5;
	float m_fX = 512 - 160 ;
	float m_fY = 0;
	bool m_bResetAnimation = true;
	int AnimationCounter = 0;
	int EnemyAnimationsX = 5;
	int EnemyAnimationsY = 6;
	int Size = 13;
};

