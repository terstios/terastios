#include "stdafx.h"
#include "EarthBossSpear.h"



EarthBossSpear::EarthBossSpear()
{
	m_bVisible = true;
	rect.setSize(sf::Vector2f(48 * sprite.getScale().x, 48 * sprite.getScale().y));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(AnimationsX * 64,AnimationsY * 64, 64, 64));
}


EarthBossSpear::~EarthBossSpear()
{
}


void EarthBossSpear::SetVisible()
{
	m_bVisible = false;
}

void EarthBossSpear::update()
{
	if (Deathcounter == 0)
	{
		Erase = true;
	}
	Deathcounter--;
	if (AnimationCounter == 4)
	{
		AnimationsX++;
		AnimationCounter = 0;

	}
	AnimationCounter++;

	if (AnimationsX * 128 >= 128 * Size)
	{
		AnimationsX = 0;
		AnimationsY++;
	}
	
	if (AnimationsX == 2 && AnimationsY == 3)
	{
		m_bHitboxactive = true;
		rect.setPosition(sprite.getPosition().x + 8, sprite.getPosition().y + 16);
	}
		
	else if (AnimationsX == 0 && AnimationsY == 4)
	{
		m_bHitboxactive = false;
		rect.setPosition(0, 0);
	}
		

	sprite.setTextureRect(sf::IntRect(AnimationsX * 64, AnimationsY * 64, 64, 64));
	sprite.setPosition(PlayerX, PlayerY);
}

void EarthBossSpear::Shoot(float p_fPlayerPosX, float p_fPlayerPosY)
{
	PlayerX = p_fPlayerPosX;
	PlayerY = p_fPlayerPosY;
}

bool EarthBossSpear::IsActive()
{
	if (m_bHitboxactive)
	{
		return true;
	}
	else
		return false;
}

int EarthBossSpear::ReturnX()
{
	return rect.getPosition().x;
}

int EarthBossSpear::ReturnY()
{
	return rect.getPosition().y;
}

