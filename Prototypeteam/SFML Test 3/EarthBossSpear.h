#pragma once
#include "iEntity.h"

class EarthBossSpear : public iEntity
{
public:
	EarthBossSpear();
	~EarthBossSpear();

	void SetVisible();
	void update();
	void Shoot(float p_fPlayerPosX, float p_fPlayerPosY);
	bool IsActive();
	int ReturnX();
	int ReturnY();
	bool Erase = false;
private:
	int Deathcounter = 600;
	bool m_bHitboxactive = false;
	float PlayerX = 0;
	float PlayerY = 0;
	bool m_bVisible;
	int AnimationsX = 0;
	int AnimationsY = 0;
	int AnimationCounter = 0;
	int Size = 4;
};

