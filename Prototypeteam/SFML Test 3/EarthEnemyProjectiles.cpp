#include "stdafx.h"
#include "EarthEnemyProjectiles.h"



EarthEnemyProjectiles::EarthEnemyProjectiles()
{
	m_bVisible = true;
	rect.setSize(sf::Vector2f(16, 16));
	rect.setFillColor(sf::Color::White);
}


EarthEnemyProjectiles::~EarthEnemyProjectiles()
{
}

bool EarthEnemyProjectiles::CheckHit()
{
	if (m_bVisible == false)
		return false;
	else
		return true;
}
void EarthEnemyProjectiles::SetVisible()
{
	m_bVisible = false;
}

void EarthEnemyProjectiles::update()
{
	if (Deathcounter == 0)
	{
		Erase = true;
	}
	Deathcounter--;
	sprite.setPosition(sf::Vector2f(EarthEnemyProjectileX += cosine * 10 , EarthEnemyProjectileY += sine * 10 ));
	rect.setPosition(sprite.getPosition().x, sprite.getPosition().y);
	
}
// TODO f� enemy projektiler att spawna p� shooter enemies
void EarthEnemyProjectiles::fire(float p_fShooterEnemyPosY, float p_fShooterEnemyPosX, float p_fPlayerPosY, float p_fPlayerPosX)
{

	Angle = atan2(p_fPlayerPosY - p_fShooterEnemyPosY + 32,
		p_fPlayerPosX - p_fShooterEnemyPosX + 32);

	Angle * 180 / PI;
	cosine = cos(Angle);// Note: This angle is in radians.
	sine = sin(Angle);

	EarthEnemyProjectileX = p_fShooterEnemyPosX ;
	EarthEnemyProjectileY = p_fShooterEnemyPosY ;

} 

void EarthEnemyProjectiles::SetSpeedBoost(float p_fSpeed)
{
	SpeedBoost = p_fSpeed;
}