#pragma once
#include "iEntity.h"

class EarthEnemyProjectiles : public iEntity
{
public:
	EarthEnemyProjectiles();
	~EarthEnemyProjectiles();
	bool CheckHit();
	void SetVisible();
	void update();
	void fire(float p_fShooterEnemyPosY, float p_fShooterEnemyPosX, float p_fPlayerPosY, float p_fPlayerPosX);

	void SetSpeedBoost(float p_fSpeed);
	bool Erase = false;
private:
	int Deathcounter = 600;
	float SpeedBoost = 0;
	float PI = 3.14159265;
	float PlayerX = 0;
	float PlayerY = 0;
	float EarthEnemyProjectileX;
	float EarthEnemyProjectileY;
	float cosine = 0;
	float sine = 0;
	float Angle = 0;
	bool m_bVisible;
};

