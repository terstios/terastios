#include "stdafx.h"
#include "Enemy.h"



Enemy::Enemy()
{
	sprite.setScale(2, 2);
	m_bVisible = true;
	rect.setSize(sf::Vector2f(32 * sprite.getScale().x,32 * sprite.getScale().y));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(EnemyAnimationsX * 64, EnemyAnimationsY * 64, 64, 64));
}


Enemy::~Enemy()
{
}

void Enemy::update()
{
	if (Deathcounter == 0)
	{
		Erase = true;
	}
	Deathcounter--;

	if (AnimationCounter == 4)
	{
		EnemyAnimationsX++;
		AnimationCounter = 0;

	}
	AnimationCounter++;

	sprite.setPosition(m_fX, m_fY);
	rect.setPosition(sprite.getPosition().x + 16 * sprite.getScale().x, sprite.getPosition().y + 16 * sprite.getScale().y);
	///////////Dying animation////////////
	if (m_bDying)
	{
		if (m_bResetAnimation)
		{
			EnemyAnimationsX = 0;
			EnemyAnimationsY = 0;
			m_bResetAnimation = false;
		}
		m_fY += 5 + SpeedBoost;
		if (EnemyAnimationsX * 64 >= 64 * Size)
		{
			EnemyAnimationsX = 0;
			if (EnemyAnimationsY = 1)
				m_bVisible = false;
			EnemyAnimationsY = 1;
			
			
		}

		sprite.setTextureRect(sf::IntRect(EnemyAnimationsX * 64, EnemyAnimationsY * 64, 64, 64));
	}
	///////////While Alive////////////////
	else
	{
		m_fY += 3 + SpeedBoost;
		if (m_fY > 1024)
			m_fY = -64;

		if (EnemyAnimationsX * 64 >= 64 * Size)
		{
			EnemyAnimationsX = 0;
		}
		sprite.setTextureRect(sf::IntRect(EnemyAnimationsX * 64, EnemyAnimationsY * 64, 64, 64));

		if (sprite.getPosition().x + 32 * sprite.getScale().x > PlayerX - range - 32 && sprite.getPosition().x + 32 * sprite.getScale().x < PlayerX + range + 32 && sprite.getPosition().y + 32 * sprite.getScale().y > PlayerY - range - 32 && sprite.getPosition().y + 32 * sprite.getScale().x < PlayerY + range + 32)
		{
			if (PlayerY + 32 > sprite.getPosition().y)
			{
				EnemyAnimationsY = 3;
				Size = 7;
				if (sprite.getPosition().x + 32 * sprite.getScale().x < PlayerX - 10)
				{
					m_fX += 2;
					charging = true;
				}
				else if (sprite.getPosition().x + 32 * sprite.getScale().x > PlayerX + 10)
				{
					m_fX -= 2;
					charging = true;
				}
				

				if (charging)
				{
					
					m_fY += 4;
				}
			}
			else
			{
				EnemyAnimationsY = 2;
				Size = 8;
			}
		}
		else
		{
			EnemyAnimationsY = 2;
			Size = 8;
		}
	}
}

bool Enemy::CheckHit()
{
	if (!m_bVisible)
		return false;
	else
		return true;
}

void Enemy::SetVisible()
{
	m_bDying = true;
}

void Enemy::GetPlayerPos(float p_pPlayerposX, float p_pPlayerposY)
{
	PlayerX = p_pPlayerposX;
	PlayerY = p_pPlayerposY;
}

void Enemy::SetSpeedBoost(float p_fSpeed)
{
	SpeedBoost = p_fSpeed;
}

void Enemy::SetPos(float NewPosX, float NewPosY)
{
	m_fX = NewPosX;
	m_fY = NewPosY;
}
