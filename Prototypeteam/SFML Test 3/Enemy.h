#pragma once
#include "iEntity.h"

class Enemy : public iEntity
{
public:
	Enemy();
	~Enemy();
	void update();
	bool CheckHit();
	void SetVisible();
	void GetPlayerPos(float p_pPlayerposX, float p_pPlayerposY);
	void SetSpeedBoost(float p_fSpeed);
	void SetPos(float NewPosX, float NewPosY);
	bool Erase = false;
private:
	int Deathcounter = 600;
	bool charging = false;
	float SpeedBoost = 0;
	float PlayerX;
	float PlayerY;
	int range = 256;
	float m_fSpeed;
	float m_fX = 480;
	float m_fY = -64;
	bool m_bVisible;
	int Size = 8;
	bool m_bDying = false;
	bool m_bResetAnimation = true;
	int AnimationCounter = 0;
	int EnemyAnimationsX = 0;
	int EnemyAnimationsY = 2;
};

