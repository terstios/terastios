#include "stdafx.h"
#include "EnemyBossProjectile.h"



EnemyBossProjectile::EnemyBossProjectile()
{
	sprite.setScale(2.5, 2.5);
	m_bVisible = true;
	rect.setSize(sf::Vector2f(29 * sprite.getScale().x, 29 * sprite.getScale().y));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(EnemyAnimationsX * 45 , EnemyAnimationsY * 43, 45, 43));
}


EnemyBossProjectile::~EnemyBossProjectile()
{
}

bool EnemyBossProjectile::CheckHit()
{
	if (m_bVisible == false)
		return false;
	else
		return true;
}

void EnemyBossProjectile::SetVisible()
{
	m_bVisible = false;
}

void EnemyBossProjectile::update()
{
	if (Deathcounter == 0)
	{
		Erase = true;
	}
	Deathcounter--;

	if (AnimationCounter == 4)
	{
		EnemyAnimationsX++;
		AnimationCounter = 0;

	}
	AnimationCounter++;

	if (EnemyAnimationsX * 45 >= 45 * 5)
	{
		EnemyAnimationsX = 0;
	}


	sprite.setPosition(sf::Vector2f(EarthBossProjectileX += cosine * 10, EarthBossProjectileY += sine * 10));
	rect.setPosition(sprite.getPosition().x + 7 * sprite.getScale().x, sprite.getPosition().y + 7 * sprite.getScale().y);
	sprite.setTextureRect(sf::IntRect(EnemyAnimationsX * 45, EnemyAnimationsY * 43, 45, 43));
}

void EnemyBossProjectile::fire(float p_fBossEnemyPosX, float p_fBossEnemyPosY, float p_fPlayerPosX, float p_fPlayerPosY)
{
	Angle = atan2(p_fPlayerPosY - p_fBossEnemyPosY + 32,
		p_fPlayerPosX - p_fBossEnemyPosX + 32);

	Angle * 180 / PI;
	cosine = cos(Angle);// Note: This angle is in radians.
	sine = sin(Angle);

	EarthBossProjectileX = p_fBossEnemyPosX - (22 + sprite.getScale().x);
	EarthBossProjectileY = p_fBossEnemyPosY - (22 + sprite.getScale().x);
}
