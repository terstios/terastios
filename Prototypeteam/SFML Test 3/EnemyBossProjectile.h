#pragma once
#include "iEntity.h"

class EnemyBossProjectile : public iEntity
{
public:
	EnemyBossProjectile();
	~EnemyBossProjectile();

	bool CheckHit();
	void SetVisible();
	void update();
	void fire(float p_fBossEnemyPosX, float p_fBossEnemyPosY, float p_fPlayerPosX, float p_fPlayerPosY);

	bool Erase = false;
private:
	int Deathcounter = 600;
	float PI = 3.14159265;
	float PlayerX = 0;
	float PlayerY = 0;
	float EarthBossProjectileX;
	float EarthBossProjectileY;
	float cosine = 0;
	float sine = 0;
	float Angle = 0;
	bool m_bVisible;
	int AnimationCounter = 0;
	int EnemyAnimationsX = 0;
	int EnemyAnimationsY = 0;
};

