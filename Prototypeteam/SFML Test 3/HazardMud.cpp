#include "stdafx.h"
#include "HazardMud.h"



HazardMud::HazardMud()
{
	rect.setSize(sf::Vector2f(48 * sprite.getScale().x, 48 * sprite.getScale().y));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(SquareX * 64, SquareY* 64, 64, 64));
}


HazardMud::~HazardMud()
{
}

void HazardMud::update()
{
	if (Deathcounter == 0)
	{
		Erase = true;
	}
	Deathcounter--;
	rect.setPosition(sprite.getPosition().x + 10, sprite.getPosition().y + 20);
	sprite.setPosition(m_fX, m_fY);
	m_fY += 5 + SpeedBoost;
	sprite.setTextureRect(sf::IntRect(SquareX * 64, SquareY * 64, 64, 64));
}

void HazardMud::SetTextureRect(int m_iXPos, int m_iYPos)
{
	SquareX = m_iXPos;
	SquareY = m_iYPos;
}

void HazardMud::SetPos(float m_fXPos, float m_fYPos)
{
	m_fX = m_fXPos;
	m_fY = m_fYPos;
}

void HazardMud::SetSpeedBoost(float p_fSpeed)
{
	SpeedBoost = p_fSpeed;
}

void HazardMud::SetSlowed()
{
	slowed = true;
}

void HazardMud::SetSlowedFalse()
{
	slowed = false;
}

bool HazardMud::GetSlowed()
{
	return slowed;
}


