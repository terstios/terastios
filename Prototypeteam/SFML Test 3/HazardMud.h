#pragma once
#include "iEntity.h"

class HazardMud : public iEntity
{
public:
	HazardMud();
	~HazardMud();

	void update();
	void SetTextureRect(int m_iXPos, int m_iYPos);
	void SetPos(float m_fXPos, float m_fYPos);
	void SetSpeedBoost(float p_fSpeed);
	void SetSlowed();
	void SetSlowedFalse();
	bool GetSlowed();
	bool Erase = false;
private:
	int Deathcounter = 600;
	bool slowed = false;
	float SpeedBoost = 0;
	float m_fX = 0;
	float m_fY = 0;
	int SquareX = 0;
	int SquareY = 0;
};

