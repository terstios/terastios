#include "stdafx.h"
#include "Hourglass.h"



Hourglass::Hourglass()
{
	rect.setSize(sf::Vector2f(0, 0));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(AnimationsX * 64, AnimationsY * 128, 64, 128));
	sprite.setPosition(5, 640);
}


Hourglass::~Hourglass()
{
}

void Hourglass::update()
{
	if (AnimationCounter >= Time * 60)
	{
		AnimationsY++;
		AnimationCounter = 0;
		FaceCounter++;
		if (FaceCounter == 6)
		{
			FaceCounter = 5;
		}
		if (AnimationsY == 5)
		{
			if (!once)
			{
				CurrentHealthCheck = PlayerHealth;
				once = true;
				ActiveHeart = true;
			}
		}
		
		
	}
	AnimationCounter++;

	

	if (!ActiveHeart)
	{
		if (PlayerHealth >= 4)
		{
			AnimationCounter++;
		}
		if (PlayerHealth >= 3)
		{
			AnimationCounter++;
		}
		if (PlayerHealth >= 2)
		{
			AnimationCounter++;
		}
		if (PlayerHealth >= 1)
		{
			AnimationCounter++;
		}
	}
	
	if (PlayerHealth >= 1)
	{
		AnimationsX = 1;
	}
	if (PlayerHealth >= 2)
	{
		AnimationsX = 2;
	}
	if (PlayerHealth >= 3)
	{
		AnimationsX = 3;
	}
	if (PlayerHealth >= 4)
	{
		AnimationsX = 4;
	}
	/*if (PlayerHealth >= 5)
	{
		AnimationsX = 5;
	}*/
	
	if (ActiveHeart)
	{
		if (PlayerHealth >= CurrentHealthCheck + 2)
		{
			AnimationsY = 3;
			AnimationsX = 5;
		}
		else if (PlayerHealth >= CurrentHealthCheck + 1)
		{
			AnimationsY = 2;
			AnimationsX = 5;
		}
		else if (PlayerHealth >= CurrentHealthCheck)
		{
			AnimationsY = 1;
			AnimationsX = 5;
		}
	}




	sprite.setTextureRect(sf::IntRect(AnimationsX * 64, AnimationsY * 128, 64, 128));

}

void Hourglass::GetHealth(int Health)
{
	PlayerHealth = Health;
}

int Hourglass::ReturnFaceCounter()
{
	return FaceCounter;
}
