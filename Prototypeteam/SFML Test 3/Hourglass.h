#pragma once
#include "iEntity.h"

class Hourglass : public iEntity
{
public:
	Hourglass();
	~Hourglass();
	void update();
	void GetHealth(int Health);
	int ReturnFaceCounter();
private:
	int FaceCounter = 0;
	bool once = false;
	int CurrentHealth = 0;
	int CurrentHealthCheck = 0;
	bool ActiveHeart = false;
	int PlayerHealth = 0;
	float m_fX;
	float m_fY;
	int AnimationCounter = 0;
	int AnimationsX = 0;
	int AnimationsY = 0;
	int Time = 10;

};

