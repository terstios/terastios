#include "stdafx.h"
#include "HudFace.h"



HudFace::HudFace()
{

	sprite.setTextureRect(sf::IntRect(faceCounter * 64, 0, 64, 64));
}


HudFace::~HudFace()
{
}

void HudFace::update()
{
	sprite.setPosition(m_fX, m_fY);
	if (faceCounter == -1)
		faceCounter = 0;
	
	sprite.setTextureRect(sf::IntRect(faceCounter * 64, 0, 64, 64));
}

void HudFace::GetFaceCounter(int face)
{
	faceCounter = face - 1;
}
