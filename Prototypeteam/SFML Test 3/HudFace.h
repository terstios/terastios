#pragma once
#include "iEntity.h"

class HudFace : public iEntity
{
public:
	HudFace();
	~HudFace();


	void update();
	void GetFaceCounter(int face);

private:
	float m_fX = 5;
	float m_fY = 768 - 202;
	int faceCounter = 0;
};

