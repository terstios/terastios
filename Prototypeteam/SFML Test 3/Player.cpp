#include "stdafx.h"
#include "Player.h"



Player::Player()
{
	rect.setSize(sf::Vector2f(16, 32));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(AnimationsX * 64, AnimationsY * 64, 64, 64));
	color = sprite.getColor();
}


Player::~Player()
{
}

void Player::update()
{
	
	if (AnimationCounter == 4)
	{
		AnimationsX++;
		AnimationCounter = 0;

	}
	AnimationCounter++;

	if (AttackDelay == 4)
	{
		AttackDelay = 0;
	}
	AttackDelay++;

	if (AnimationsX == 4 && AnimationsY == 8)
	{
		ReadyToFire = true;
	}
	else
	{
		ReadyToFire = false;
	}
	

	sprite.setTextureRect(sf::IntRect(AnimationsX * 64, AnimationsY * 64, 64, 64));
	sprite.setPosition(m_fPlayerX, m_fPlayerY);
	rect.setPosition(sprite.getPosition().x + rect.getSize().x / 2 + 20, sprite.getPosition().y + rect.getSize().y / 2);
	if (invincibilityFrames > 0)
		invincibilityFrames--;
	//////Invicibility frames stuff/////
	{
		if (invincibilityFrames >= 105)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 150;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 90)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 255;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 75)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 150;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 60)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 255;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 45)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 150;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 30)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 255;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 15)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 150;
			sprite.setColor(color);
		}
		else
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 255;
			sprite.setColor(color);
		}
		
	}

	if(PowerDuration > 0)
		PowerDuration--;


}

void Player::updateMovement()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		m_fPlayerY += -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		m_fPlayerX += -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		m_fPlayerY += speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		m_fPlayerX += speed;
	}
}

void Player::setPosition(float p_fX, float p_fY)
{
	m_fPlayerX = p_fX;
	m_fPlayerY = p_fY;
}

int Player::returnHealth()
{
	return m_iHealth;
}

void Player::setHealth()
{
	if (invincibilityFrames <= 0)
	{
		invincibilityFrames = 120;
		m_iHealth++;
	}
}

int Player::returnPowerDuration()
{
	return PowerDuration;
}

bool Player::resetPowerDuration()
{
	if (PowerDuration == 0)
	{
		PowerDuration = 300;
		return true;
	}
	else
		return false;
}

void Player::Attacking()
{
	AnimationsX = 0;
	AnimationsY = 8;
}

void Player::PushPlayertoTop()
{
	m_fPlayerY += -6;
}

void Player::Damaged()
{
	m_bDamaged = true;
	counter = 0;
}
