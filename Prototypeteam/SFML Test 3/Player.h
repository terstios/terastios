#pragma once
#include "iEntity.h"



class Player : public iEntity
{
public:
	Player();
	~Player();

	void update();
	void updateMovement();
	void setPosition(float p_fX, float p_fY);
	int returnHealth();
	void setHealth();
	int returnPowerDuration();
	bool resetPowerDuration();
	void Attacking();
	void PushPlayertoTop();
	void Damaged();
	sf::Color color;
	bool ReadyToFire = false;

private:

	int AttackDelay = 0;
	int PowerDuration = 0;
	int invincibilityFrames = 0;
	int speed = 4;
	int counter = 0;
	bool m_bDamaged = false;
	int m_iHealth = 0;
	float m_fPlayerX = 484;
	float m_fPlayerY = 600;
	int AnimationCounter = 0;
	int AnimationsX = 0;
	int AnimationsY = 10;
	int Size = 8;
};

