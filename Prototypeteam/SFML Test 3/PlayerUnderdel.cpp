#include "stdafx.h"
#include "PlayerUnderdel.h"



PlayerUnderdel::PlayerUnderdel()
{
	rect.setSize(sf::Vector2f(16, 32));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(AnimationsX * 64, AnimationsY * 64, 64, 64));
	color = sprite.getColor();
}


PlayerUnderdel::~PlayerUnderdel()
{
}

void PlayerUnderdel::update()
{
	if (AnimationCounter == 4)
	{
		AnimationsX++;
		AnimationCounter = 0;
		

	}
	AnimationCounter++;

	if (Bossfightmode == true)
	{
		if (AnimationsX * 64 >= 64 * Size)
		{

			AnimationsX = 0;

			AnimationsY = 11;
			Size = 12;
		}
	}
	else
	{
		if (AnimationsX * 64 >= 64 * Size)
		{

			AnimationsX = 0;

			AnimationsY = 9;

		}
	}
	

	if (AnimationsX == 8)
		AttackingSprite = 0;


	if (invincibilityFrames > 0)
		invincibilityFrames--;
	//////Invicibility frames stuff/////
	{
		if (invincibilityFrames >= 105)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 150;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 90)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 255;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 75)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 150;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 60)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 255;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 45)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 150;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 30)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 255;
			sprite.setColor(color);
		}
		else if (invincibilityFrames >= 15)
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 150;
			sprite.setColor(color);
		}
		else
		{
			sf::Color color;
			color = sprite.getColor();
			color.a = 255;
			sprite.setColor(color);
		}
		
	}

	sprite.setPosition(m_fX, m_fY);
	sprite.setTextureRect(sf::IntRect(AnimationsX * 64, (AttackingSprite + AnimationsY) * 64, 64, 64));
}

void PlayerUnderdel::Attacking()
{
	AttackingSprite = 1;
	AnimationsX = 0;
}

void PlayerUnderdel::Damaged()
{
	if (invincibilityFrames <= 0)
	{
		invincibilityFrames = 120;
		
	}
}

void PlayerUnderdel::SetPlayerPos(float X, float Y)
{
	m_fX = X;
	m_fY = Y;
}

void PlayerUnderdel::HandleEvents()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		AnimationsY = 9;
		Size = 16;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		AnimationsY = 6;
		Size = 12;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		AnimationsY = 0;
		Size = 10;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		AnimationsY = 2;
		Size = 10;
	}
}
