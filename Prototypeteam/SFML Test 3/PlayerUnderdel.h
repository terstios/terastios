#pragma once
#include "iEntity.h"


class PlayerUnderdel : public iEntity
{
public:
	PlayerUnderdel();
	~PlayerUnderdel();

	void update();
	void Attacking();
	void Damaged();
	void SetPlayerPos(float X, float Y);
	void HandleEvents();
	sf::Color color;
	bool Bossfightmode = false;
private:
	int AttackingSprite = 0;
	bool m_bDamaged = false;
	int invincibilityFrames = 0;
	int counter = 0;
	float m_fX = 0;
	float m_fY = 0;
	int flash = 0;
	int Size = 16;
	int AnimationCounter = 0;
	int AnimationsX = 0;
	int AnimationsY = 9;
};

