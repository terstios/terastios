#pragma once
#include "iEntity.h"

class PowerupPowerShot : public iEntity
{
public:
	PowerupPowerShot();
	~PowerupPowerShot();

	void update();
	bool CheckHit();
	void SetVisible();
	void SetPosition(float EnemyX, float EnemyY);
	void SetSpeedBoost(float p_fSpeed);
	bool Erase = false;
private:
	int Deathcounter = 600;
	float SpeedBoost = 0;
	float m_fX = 300;
	float m_fY = 300;
	int AnimationCounter = 0;
	int AnimationsX = 0;
	int AnimationsY = 0;
	int Size = 10;
	bool m_bVisible;
};

