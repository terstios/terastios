#include "stdafx.h"
#include "PowerupSpeedBoost.h"



PowerupSpeedBoost::PowerupSpeedBoost()
{
	sprite.setScale(1.5, 1.5);
	m_bVisible = true;
	rect.setSize(sf::Vector2f(16 * sprite.getScale().x, 16 * sprite.getScale().y));
	rect.setFillColor(sf::Color::White);
	sprite.setTextureRect(sf::IntRect(AnimationsX * 32, AnimationsY * 64, 32, 64));
}


PowerupSpeedBoost::~PowerupSpeedBoost()
{
}

void PowerupSpeedBoost::update()
{
	if (Deathcounter == 0)
	{
		Erase = true;
	}
	Deathcounter--;

	if (AnimationCounter == 4)
	{
		AnimationsX++;
		AnimationCounter = 0;
	}
	AnimationCounter++;


	sprite.setPosition(m_fX, m_fY);
	rect.setPosition(sprite.getPosition().x + 8 * sprite.getScale().x, sprite.getPosition().y + 24 * sprite.getScale().y);
	m_fY += 1 + SpeedBoost;
	if (m_fY > 1024)
		m_fY = -64;

	if(AnimationsX * 32 >= 32 * Size)
	{
		AnimationsY = 1;
		AnimationsX = 0;
		Size = 14;
	}

	sprite.setTextureRect(sf::IntRect(AnimationsX * 32, AnimationsY * 64, 32, 64));
}

bool PowerupSpeedBoost::CheckHit()
{
	if (!m_bVisible)
		return false;
	else
		return true;
}

void PowerupSpeedBoost::SetVisible()
{
	m_bVisible = false;
}

void PowerupSpeedBoost::SetPosition(float EnemyX, float EnemyY)
{
	m_fX = EnemyX;
	m_fY = EnemyY;
}

void PowerupSpeedBoost::SetSpeedBoost(float p_fSpeed)
{
	SpeedBoost = p_fSpeed;
}