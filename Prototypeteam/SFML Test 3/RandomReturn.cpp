#include "stdafx.h"
#include "RandomReturn.h"
#include <ctime>



RandomReturn::RandomReturn()
{
	srand(time(NULL));
}


RandomReturn::~RandomReturn()
{
}

bool RandomReturn::ReturnRandom50()
{
	int randomcheck = 1 +rand() % 2;

	if (randomcheck == 0)
	{
		return true;
	}
	if (randomcheck == 1)
	{
		return false;
	}
}

int RandomReturn::ReturnRandom1to4()
{
	int randomcheck = 1 + rand() % 4;

	if (randomcheck == 1)
	{
		return 1;
	}
	if (randomcheck == 2)
	{
		return 2;
	}
	if (randomcheck == 3)
	{
		return 3;
	}
	if (randomcheck == 4)
	{
		return 4;
	}
}

int RandomReturn::ReturnRandom1to3()
{
	int randomcheck = 1 + rand() % 3;

	if (randomcheck == 1)
	{
		return 1;
	}
	if (randomcheck == 2)
	{
		return 2;
	}
	if (randomcheck == 3)
	{
		return 3;
	}
}
