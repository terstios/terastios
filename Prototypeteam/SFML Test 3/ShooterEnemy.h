#pragma once
#include "iEntity.h"
#include "RandomReturn.h"


class ShooterEnemy : public iEntity
{
public:
	ShooterEnemy();
	~ShooterEnemy();
	void update();
	bool CheckHit();
	void SetVisible();
	void SetSpeedBoost(float p_fSpeed);
	void SetPos(float NewPosX, float NewPosY);
	class RandomReturn randomShooterChecker;
	bool CheckFire();
	bool Erase = false;
private:
	int Deathcounter = 600;
	bool m_bDying = false;
	int AttackDelay = 0;
	bool ActiveAttack = false;
	bool Attack = false;
	bool m_bFire = false;
	int ChilloutTimer = 0;
	float SpeedBoost = 0;
	bool RandomCheck = false;
	float m_fSpeed;
	float m_fX = 480;
	float m_fY = -64;
	bool m_bVisible;
	int Size = 12;
	int AnimationCounter = 0;
	int EnemyAnimationsX = 0;
	int EnemyAnimationsY = 0;
};

