#include "stdafx.h"
#include "iEntity.h"
#include "Arrow.h"
#include "Player.h"
#include "Enemy.h"
#include "PowerupRainOfArrow.h"
#include "Hourglass.h"
#include "ShooterEnemy.h"
#include "EarthEnemyProjectiles.h"
#include "PowerupRapidFire.h"
#include "PowerupPowerShot.h"
#include "PowerupSpeedBoost.h"
#include "EarthBossEnemy.h"
#include "HazardMud.h"
#include "EnemyBossProjectile.h"
#include "EarthBossSpear.h"
#include "RandomReturn.h"
#include "BossCrusherArms.h"
#include "BeamerEnemy.h"
#include "BeamProjectile.h"
#include "PlayerUnderdel.h"
#include "HudFace.h"

int SCREENWIDTH = 1024;
int SCREENHEIGHT = 768;
float PlayerX = 484;
float PlayerY = 600;
int AnimationCounter = 0;
bool playerFire = false;
bool enemyFire = false;
void HandleEvents();
bool drawarrow;
bool drawenemyprojectile;
float dx;
float dy;
float Rotation;
float BackgroundX[4] = {0, 256, 512, 768 };
float BackgroundY[5] = {512, 256, 0, -255, -511 };
float BackgroundSideY[5] = { 512, 256, 0, -255, -511 };
float BackgroundSideX[2] = {0, 768};
int counter = 0;
int counter2 = 0;
int counter3 = 0;
int arrowCounter = 60;
int golemCounter = 60;
int EnemyProjectileCounter = 60;
int EnemyProjectileSpawnCounter = -1;
int GolemSpawnCounter = -1;
int proaCounter = 60;
int proaSpawnCounter = -1;
int rfSpawnCounter = -1;
int psSpawnCounter = -1;
int sbSpawnCounter = -1;
int ArrowSpawnCounter = -1;
int ShooterEnemySpawnCounter = -1;
int ShooterEnemyCounter = 60;
int BossProjSpawnCounter = -1;
int BossSpearSpawnCounter = -1;
int BossCrushspawnCounter = -1;
int BeamspawnCounter = -1;
int BeamerSpawnCounter = -1;
int MudSpawnCounter = -1;
bool hitAll = false;
int rapidFireCooldown = 60;
bool PowerShotCooldown = false;
float SpeedBoostPower = 5;
float SpeedBoostSlow = 0;
bool Slowed = false;
float distanceRunning = 0;
int SpawnCounter = 1;
float LeftSpawn = 200;
float MiddleSpawn = 400;
float RightSpawn = 700;
float SpawnPosition[] = { -200, -264, -328 };
int BossAttackDelay = 0; 
int BeamerAttackDelay = 0;
bool LineAttackbackwards = false;
bool LineAttackForwards = false;
int LineAttackCounterBackwards = 0;
int LineAttackCounter2Backwards = 0;
int LineAttackCounterForwards = 0;
int LineAttackCounter2Forwards = 0;
bool Options = false;
bool Activateboss = false;
bool Playstate = false;
bool PrePlaystate = false;
int MoveBackground = 0;
int MoveBackgroundLeft = 0;
int MoveBackgroundRight = 0;
bool BackgroundOnce = false;
bool IntroplayerActivate = false;
bool PreBossFight = false;
bool BossFightState = false;
bool ClearArraysOnce = false;

int Score = 0;
int scoreCalc;
int WhatPowerup = 4;
int AnimationsX = 0;
int AnimationsY = 0;
int Poweruprandomiser = 0;
int DistanceCounter = 0;
float FadeoutCounter = 0;
int ArrowcooldownCounter = 4;
int CrosshairAnimationX = 0;
int CrosshairAnimationY = 0;
int CrosshairAnimationCounter = 0;
bool CrosshairAnimationbool = false;

int IntroEnemyAnimationCounter = 0;
int IntroAnimationCounter = 0;
int IntroAnimationX = 0;
int IntroAnimationY = 9;
float IntroCamaronY = 1068;
int IntroEnemyAnimationY = 0;
bool MovebackFromRight = false;
bool FullscreenActive = false;


//////////////////////////////
//////////DRAW HITBOX/////////
/**/bool DrawHitbox = false;//
/**/bool PlayMusic = false;//
/**/bool Menu = true;////////
//////////////////////////////

//////////////////////////////
/**/ int SpawnArray[] { 0, 0, 0, 0, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, };

//////////////////////////////



int main(int argc, char** argv)
{
	sf::RenderWindow window(sf::VideoMode(SCREENWIDTH, SCREENHEIGHT), "Agtoschmemp", sf::Style::Titlebar | sf::Style::Close);
	
	
	window.setFramerateLimit(60);

	sf::Music backgroundmusic;
	sf::SoundBuffer Deathsoundbuffer;
	sf::SoundBuffer Arrowsoundbuffer;
	sf::SoundBuffer RainofArrowssoundbuffer;
	sf::SoundBuffer Menusoundbuffer;


	sf::Sprite BossArenaScreen;
	sf::Sprite StartButtonSprite;
	sf::Sprite OptionsButtonSprite;
	sf::Sprite ScoreButtonSprite;
	sf::Sprite QuitButtonSprite;
	sf::Sprite StartButtonSpriteIdle;
	sf::Sprite OptionsButtonSpriteIdle;
	sf::Sprite ScoreButtonSpriteIdle;
	sf::Sprite QuitButtonSpriteIdle;
	sf::Sprite CrosshairSprite;
	sf::Sprite EnemySprites;
	sf::Sprite backgroundMenuSprite;
	sf::Sprite FullscreenCross;
	sf::Sprite SoundCross;
	sf::Sprite BackArrowOptions;
	sf::Sprite BackArrowOptionsSelected;
	sf::Sprite MainMenuText;
	sf::Sprite MainMenuScreenGlowing;
	sf::Sprite OptionsText;
	sf::Sprite OptionsSettings;
	sf::RectangleShape BlackoutSquare;
	sf::RectangleShape Leftwall;
	sf::RectangleShape Rightwall;
	sf::RectangleShape Menurect1;
	sf::RectangleShape Menurect2;
	sf::RectangleShape Menurect3;
	sf::RectangleShape Menurect4;
	sf::RectangleShape Menurect5;
	sf::RectangleShape Menurect6;
	sf::RectangleShape Menurect7;
	sf::Sprite ButtonIdle;
	sf::Sprite ButtonActive;
	sf::RectangleShape cursorRect;
	sf::Sprite Score1;
	sf::Sprite Score2;
	sf::Sprite Score3;
	sf::Sprite Score4;
	sf::Sprite Score5;
	sf::Sprite PowerupHUDSprite;
	sf::Sprite BackgroundSpriteX0Y0;
	sf::Sprite BackgroundSpriteX0Y1;
	sf::Sprite BackgroundSpriteX0Y2;
	sf::Sprite BackgroundSpriteX0Y3;
	sf::Sprite BackgroundSpriteX0Y4;
	sf::Sprite BackgroundSpriteX1Y0;
	sf::Sprite BackgroundSpriteX1Y1;
	sf::Sprite BackgroundSpriteX1Y2;
	sf::Sprite BackgroundSpriteX1Y3;
	sf::Sprite BackgroundSpriteX1Y4;
	sf::Sprite BackgroundSpriteX2Y0;
	sf::Sprite BackgroundSpriteX2Y1;
	sf::Sprite BackgroundSpriteX2Y2;
	sf::Sprite BackgroundSpriteX2Y3;
	sf::Sprite BackgroundSpriteX2Y4;
	sf::Sprite BackgroundSpriteX3Y0;
	sf::Sprite BackgroundSpriteX3Y1;
	sf::Sprite BackgroundSpriteX3Y2;
	sf::Sprite BackgroundSpriteX3Y3;
	sf::Sprite BackgroundSpriteX3Y4;
	sf::Sprite BackgroundSidesSpriteX0Y0;
	sf::Sprite BackgroundSidesSpriteX0Y1;
	sf::Sprite BackgroundSidesSpriteX0Y2;
	sf::Sprite BackgroundSidesSpriteX0Y3;
	sf::Sprite BackgroundSidesSpriteX0Y4;
	sf::Sprite BackgroundSidesSpriteX1Y0;
	sf::Sprite BackgroundSidesSpriteX1Y1;
	sf::Sprite BackgroundSidesSpriteX1Y2;
	sf::Sprite BackgroundSidesSpriteX1Y3;
	sf::Sprite BackgroundSidesSpriteX1Y4;
	sf::Sprite HUDPowerupSprite;
	sf::Sprite OptionsMenu;
	sf::Sprite MenuCross;
	sf::Sprite IntroScreen;
	sf::Sprite IntroCamaron;
	sf::Sprite EnemiesWalking;


	sf::Texture PowerupRainOfArrowTexture;
	sf::Texture PlayerTexture;
	sf::Texture CrosshairTexture;
	sf::Texture ArrowTexture;
	sf::Texture EnemyTextures;
	sf::Texture EarthEnemyRunningTexture;
	sf::Texture BackgroundMenuTexture;
	sf::Texture ButtonIdleTexture;
	sf::Texture ButtonActiveTexture;
	sf::Texture BackgroundTexture;
	sf::Texture BackgroundSideTexture;
	sf::Texture PowerupHUDTexture;
	sf::Texture HourglassTexture;
	sf::Texture HUDPowerupTexture;
	sf::Texture ShooterEnemyTexture;
	sf::Texture EarthEnemyProjectileTexture;
	sf::Texture PowerupRapidFireTexture;
	sf::Texture PowerupPowerShotTexture;
	sf::Texture PowerupSpeedBoostTexture;
	sf::Texture EarthBossEnemyTexture;
	sf::Texture MudTexture;
	sf::Texture EnemyBossProjectileTexture;
	sf::Texture EnemyBossSpearTexture;
	sf::Texture EnemyBossCrushTexture;
	sf::Texture BeamerEnemyTexture;
	sf::Texture BeamProjectileTexture;
	sf::Texture HudFaceTexture;
	sf::Texture StartButtonTexture;
	sf::Texture OptionsButtonTexture;
	sf::Texture ScoreButtonTexture;
	sf::Texture QuitButtonTexture;
	sf::Texture StartButtonTextureIdle;
	sf::Texture OptionsButtonTextureIdle;
	sf::Texture ScoreButtonTextureIdle;
	sf::Texture QuitButtonTextureIdle;
	sf::Texture OptionsMenuTexture;
	sf::Texture MenuCrossTexture;
	sf::Texture IntroScreenTexture;
	sf::Texture EnemywalkingTexture;
	sf::Texture CrossTexture;
	sf::Texture OptionsTextTexture;
	sf::Texture MenuTextTexture;
	sf::Texture MenuTextTextureGlowing;
	sf::Texture OptionsSettingTexture;
	sf::Texture OptionsBackTexture;
	sf::Texture OptionsBackTextureSelected;
	sf::Texture BossArenaScreenTexture;



	sf::Vector2i PlayerAnimation(64, 0);
	sf::Texture ScoreTexture;

	sf::Sound Menusound;
	sf::Sound Arrowsound;
	sf::Sound Deathsound;
	sf::Sound RoAsound;


	BlackoutSquare.setSize(sf::Vector2f(1024, 768));
	BlackoutSquare.setFillColor(sf::Color::Black);
	cursorRect.setSize(sf::Vector2f(16, 16));
	Menurect1.setSize(sf::Vector2f(160, 64));
	Menurect1.setFillColor(sf::Color::White);
	Menurect2.setSize(sf::Vector2f(160, 64));
	Menurect2.setFillColor(sf::Color::White);
	Menurect3.setSize(sf::Vector2f(160, 64));
	Menurect3.setFillColor(sf::Color::White);
	Menurect4.setSize(sf::Vector2f(160, 64));
	Menurect4.setFillColor(sf::Color::White);
	Menurect5.setSize(sf::Vector2f(102, 30));
	Menurect5.setFillColor(sf::Color::White);
	Menurect6.setSize(sf::Vector2f(64, 64));
	Menurect6.setFillColor(sf::Color::White);
	Menurect7.setSize(sf::Vector2f(64, 64));
	Menurect7.setFillColor(sf::Color::White);
	Leftwall.setSize(sf::Vector2f(32, 1024));
	Rightwall.setSize(sf::Vector2f(32, 1024));
	

	sf::Color BlackoutScreen;
	BlackoutScreen = BlackoutSquare.getFillColor();
	BlackoutScreen.a = 0;
	

	///////// SOUNDS //////////
	{
		if (!backgroundmusic.openFromFile("../MusicAssets/Test Music.wav"))
			return -1; // error
		{
			backgroundmusic.setVolume(5);
			if(PlayMusic)
			backgroundmusic.play();
		}
		if (!Deathsoundbuffer.loadFromFile("../MusicAssets/Death Sound.wav"))
			return -1; // error
		{
			Deathsound.setVolume(25);
			Deathsound.play();
		}
		if (!Arrowsoundbuffer.loadFromFile("../MusicAssets/Arrow Sound3.wav"))
			return -1; // error
		{
			Arrowsound.play();
		}

		if (!RainofArrowssoundbuffer.loadFromFile("../MusicAssets/Rain Of Arrows Sound.wav"))
			return -1; // error
		{
			RoAsound.play();
		}
		if (!Menusoundbuffer.loadFromFile("../MusicAssets/Menu Button Sound2.wav"))
			return -1; // error
		{
			Menusound.play();
		}
	}

	/////////////////Load Image///////////////////////
	{
		if (!PlayerTexture.loadFromFile("../Assets/Camaron.png"))
		{
			window.close();
		}
		if (!CrosshairTexture.loadFromFile("../Assets/Crosshair2.png"))
		{
			window.close();
		}
		if (!ArrowTexture.loadFromFile("../Assets/Magic Arrow.png"))
		{
			window.close();
		}
		if (!BackgroundTexture.loadFromFile("../Assets/Maptileset middle.png"))
		{
			window.close();
		}
		if (!BackgroundSideTexture.loadFromFile("../Assets/Maptileset sides.png"))
		{
			window.close();
		}
		if (!EarthEnemyRunningTexture.loadFromFile("../Assets/Earth Enemy Sprite sheet.png"))
		{
			window.close();
		}
		if (!PowerupRainOfArrowTexture.loadFromFile("../Assets/Rain of Arrows sprite.png"))
		{
			window.close();
		}
		if (!BackgroundMenuTexture.loadFromFile("../Assets/Menu background.png"))
		{
			window.close();
		}
		if (!ButtonIdleTexture.loadFromFile("../Assets/Menu gui start 2.png"))
		{
			window.close();
		}
		if (!ButtonActiveTexture.loadFromFile("../Assets/Menu start button selected.png"))
		{
			window.close();
		}
		if (!ScoreTexture.loadFromFile("../Assets/numberus.png"))
		{
			window.close();
		}
		if (!PowerupHUDTexture.loadFromFile("../Assets/power up HUD.png", sf::IntRect(0, 0, 64, 64)))
		{
			window.close();
		}
		if (!HourglassTexture.loadFromFile("../Assets/Hourglass sprite sheet.png"))
		{
			window.close();
		}
		if (!HUDPowerupTexture.loadFromFile("../Assets/Power ups sprite sheet.png"))
		{
			window.close();
		}
		if (!ShooterEnemyTexture.loadFromFile("../Assets/Tree shooter.png"))
		{
			window.close();
		}
		if (!EarthEnemyProjectileTexture.loadFromFile("../Assets/FUCKINGOLIVE.png", sf::IntRect(0, 0, 24, 24)))
		{
			window.close();
		}
		if (!PowerupRapidFireTexture.loadFromFile("../Assets/Rapid Fire sprite.png"))
		{
			window.close();
		}
		if (!PowerupPowerShotTexture.loadFromFile("../Assets/Powershot sprite.png"))
		{
			window.close();
		}
		if (!PowerupSpeedBoostTexture.loadFromFile("../Assets/Speed sprite.png"))
		{
			window.close();
		}
		if (!EarthBossEnemyTexture.loadFromFile("../Assets/Kurt.png"))
		{
			window.close();
		}
		if (!MudTexture.loadFromFile("../Assets/Mud.png"))
		{
			window.close();
		}
		if (!EnemyBossProjectileTexture.loadFromFile("../Assets/Rolling boulder.png"))
		{
			window.close();
		}
		if (!EnemyBossSpearTexture.loadFromFile("../Assets/Ground Spear 2 row.png"))
		{
			window.close();
		}
		if (!BeamerEnemyTexture.loadFromFile("../Assets/Crystal Enemy sprite sheet.png"))
		{
			window.close();
		}
		if (!EnemyBossCrushTexture.loadFromFile("../Assets/Pillars smaller.png"))
		{
			window.close();
		}
		if (!BeamProjectileTexture.loadFromFile("../Assets/Laser.png"))
		{
			window.close();
		}
		if (!HudFaceTexture.loadFromFile("../Assets/face HUD2.png"))
		{
			window.close();
		}
		if (!StartButtonTexture.loadFromFile("../Assets/Menu start button selected.png"))
		{
			window.close();
		}
		if (!OptionsButtonTexture.loadFromFile("../Assets/Options button selected.png"))
		{
			window.close();
		}
		if (!ScoreButtonTexture.loadFromFile("../Assets/Score button selected.png"))
		{
			window.close();
		}
		if (!QuitButtonTexture.loadFromFile("../Assets/Quit button selected.png"))
		{
			window.close();
		}
		if (!StartButtonTextureIdle.loadFromFile("../Assets/Menu gui start 2.png"))
		{
			window.close();
		}
		if (!OptionsButtonTextureIdle.loadFromFile("../Assets/Menu gui options 2.png"))
		{
			window.close();
		}
		if (!ScoreButtonTextureIdle.loadFromFile("../Assets/Menu gui score 2.png"))
		{
			window.close();
		}
		if (!QuitButtonTextureIdle.loadFromFile("../Assets/Menu gui quit 2.png"))
		{
			window.close();
		}
		if (!OptionsMenuTexture.loadFromFile("../Assets/Settings screen2.png"))
		{
			window.close();
		}
		if (!MenuCrossTexture.loadFromFile("../Assets/Settings tick.png"))
		{
			window.close();
		}
		if (!IntroScreenTexture.loadFromFile("../Assets/Intro map.png"))
		{
			window.close();
		}
		if (!EnemywalkingTexture.loadFromFile("../Assets/EnemyMenuAnimation.png"))
		{
			window.close();
		}
		if (!CrossTexture.loadFromFile("../Assets/Settings tick.png"))
		{
			window.close();
		}
		if (!OptionsTextTexture.loadFromFile("../Assets/Options screen text.png"))
		{
			window.close();
		}
		if (!MenuTextTexture.loadFromFile("../Assets/Title text.png"))
		{
			window.close();
		}
		if (!MenuTextTextureGlowing.loadFromFile("../Assets/Title text2.png"))
		{
			window.close();
		}
		if (!OptionsSettingTexture.loadFromFile("../Assets/Settings.png"))
		{
			window.close();
		}
		if (!OptionsBackTexture.loadFromFile("../Assets/Back.png"))
		{
			window.close();
		}
		if (!OptionsBackTextureSelected.loadFromFile("../Assets/Back selected.png"))
		{
			window.close();
		}
		if (!BossArenaScreenTexture.loadFromFile("../Assets/Boss.png"))
		{
			window.close();
		}
	}

	/////////////////Music stuff//////////////////////
	{
		Menusound.setBuffer(Menusoundbuffer);
		RoAsound.setBuffer(RainofArrowssoundbuffer);
		Arrowsound.setBuffer(Arrowsoundbuffer);
		Deathsound.setBuffer(Deathsoundbuffer);
	}

	/////////////////Setting textures/////////////////
	{
		BossArenaScreen.setTexture(BossArenaScreenTexture);
		OptionsSettings.setTexture(OptionsSettingTexture);
		FullscreenCross.setTexture(CrossTexture);
		SoundCross.setTexture(CrossTexture);
		BackArrowOptions.setTexture(OptionsBackTexture);
		BackArrowOptionsSelected.setTexture(OptionsBackTextureSelected);
		MainMenuText.setTexture(MenuTextTexture);
		MainMenuScreenGlowing.setTexture(MenuTextTextureGlowing);
		OptionsText.setTexture(OptionsTextTexture);
		ButtonActive.setTexture(ButtonActiveTexture);
		ButtonIdle.setTexture(ButtonIdleTexture);
		backgroundMenuSprite.setTexture(BackgroundMenuTexture);
		CrosshairSprite.setTexture(CrosshairTexture);
		window.setMouseCursorVisible(false);
		StartButtonSprite.setTexture(StartButtonTexture);
		StartButtonSpriteIdle.setTexture(StartButtonTextureIdle);
		OptionsButtonSprite.setTexture(OptionsButtonTexture);
		OptionsButtonSpriteIdle.setTexture(OptionsButtonTextureIdle);
		ScoreButtonSprite.setTexture(ScoreButtonTexture);
		ScoreButtonSpriteIdle.setTexture(ScoreButtonTextureIdle);
		QuitButtonSprite.setTexture(QuitButtonTexture);
		QuitButtonSpriteIdle.setTexture(QuitButtonTextureIdle);
		IntroScreen.setTexture(IntroScreenTexture);
		IntroCamaron.setTexture(PlayerTexture);
		EnemiesWalking.setTexture(EnemywalkingTexture);



		BackgroundSpriteX0Y0.setTexture(BackgroundTexture);
		BackgroundSpriteX0Y1.setTexture(BackgroundTexture);
		BackgroundSpriteX0Y2.setTexture(BackgroundTexture);
		BackgroundSpriteX0Y3.setTexture(BackgroundTexture);
		BackgroundSpriteX0Y4.setTexture(BackgroundTexture);
		BackgroundSpriteX1Y0.setTexture(BackgroundTexture);
		BackgroundSpriteX1Y1.setTexture(BackgroundTexture);
		BackgroundSpriteX1Y2.setTexture(BackgroundTexture);
		BackgroundSpriteX1Y3.setTexture(BackgroundTexture);
		BackgroundSpriteX1Y4.setTexture(BackgroundTexture);
		BackgroundSpriteX2Y0.setTexture(BackgroundTexture);
		BackgroundSpriteX2Y1.setTexture(BackgroundTexture);
		BackgroundSpriteX2Y2.setTexture(BackgroundTexture);
		BackgroundSpriteX2Y3.setTexture(BackgroundTexture);
		BackgroundSpriteX2Y4.setTexture(BackgroundTexture);
		BackgroundSpriteX3Y0.setTexture(BackgroundTexture);
		BackgroundSpriteX3Y1.setTexture(BackgroundTexture);
		BackgroundSpriteX3Y2.setTexture(BackgroundTexture);
		BackgroundSpriteX3Y3.setTexture(BackgroundTexture);
		BackgroundSpriteX3Y4.setTexture(BackgroundTexture);
		BackgroundSidesSpriteX0Y0.setTexture(BackgroundSideTexture);
		BackgroundSidesSpriteX0Y1.setTexture(BackgroundSideTexture);
		BackgroundSidesSpriteX0Y2.setTexture(BackgroundSideTexture);
		BackgroundSidesSpriteX0Y3.setTexture(BackgroundSideTexture);
		BackgroundSidesSpriteX0Y4.setTexture(BackgroundSideTexture);
		BackgroundSidesSpriteX1Y0.setTexture(BackgroundSideTexture);
		BackgroundSidesSpriteX1Y1.setTexture(BackgroundSideTexture);
		BackgroundSidesSpriteX1Y2.setTexture(BackgroundSideTexture);
		BackgroundSidesSpriteX1Y3.setTexture(BackgroundSideTexture);
		BackgroundSidesSpriteX1Y4.setTexture(BackgroundSideTexture);
		Score1.setTexture(ScoreTexture);
		Score2.setTexture(ScoreTexture);
		Score3.setTexture(ScoreTexture);
		Score4.setTexture(ScoreTexture);
		Score5.setTexture(ScoreTexture);
		PowerupHUDSprite.setTexture(PowerupHUDTexture);
		HUDPowerupSprite.setTexture(HUDPowerupTexture);
		OptionsMenu.setTexture(OptionsMenuTexture);
		MenuCross.setTexture(MenuCrossTexture);


	}

	/////////////////CLASS OBJECTS////////////////////

	class Player player;
	player.sprite.setTexture(PlayerTexture);

	class Arrow ArrowProjectile;
	ArrowProjectile.sprite.setTexture(ArrowTexture);

	class Enemy enemy;
	enemy.sprite.setTexture(EarthEnemyRunningTexture);

	class PowerupRainOfArrow rainofArrow;
	rainofArrow.sprite.setTexture(PowerupRainOfArrowTexture);

	class Hourglass hourglass;
	hourglass.sprite.setTexture(HourglassTexture);

	class ShooterEnemy shootenemy;
	shootenemy.sprite.setTexture(ShooterEnemyTexture);

	class EarthEnemyProjectiles enemyprojectile;
	enemyprojectile.sprite.setTexture(EarthEnemyProjectileTexture);

	class PowerupRapidFire rapidFire;
	rapidFire.sprite.setTexture(PowerupRapidFireTexture);

	class PowerupPowerShot powerShot;
	powerShot.sprite.setTexture(PowerupPowerShotTexture);

	class PowerupSpeedBoost speedBoost;
	speedBoost.sprite.setTexture(PowerupSpeedBoostTexture);

	class EarthBossEnemy earthBoss;
	earthBoss.sprite.setTexture(EarthBossEnemyTexture);

	class HazardMud mudHazard;
	mudHazard.sprite.setTexture(MudTexture);

	class EnemyBossProjectile bossProjectile;
	bossProjectile.sprite.setTexture(EnemyBossProjectileTexture);

	class EarthBossSpear bossSpear;
	bossSpear.sprite.setTexture(EnemyBossSpearTexture);

	class RandomReturn randomChecker;
	
	class BossCrusherArms bossCrush;
	bossCrush.sprite.setTexture(EnemyBossCrushTexture);

	class BeamerEnemy beamerEnemy;
	beamerEnemy.sprite.setTexture(BeamerEnemyTexture);

	class BeamProjectile beamProjectile;
	beamProjectile.sprite.setTexture(BeamProjectileTexture);

	class PlayerUnderdel playerUnderdel;
	playerUnderdel.sprite.setTexture(PlayerTexture);

	class HudFace hudFace;
	hudFace.sprite.setTexture(HudFaceTexture);
	
	/////////////////vector stuff////////////////////
	
	std::vector<Enemy>::const_iterator itEnemy;
	std::vector<Enemy> enemyArray;

	std::vector<Arrow>::const_iterator itArrow;
	std::vector<Arrow> arrowArray;

	std::vector<PowerupRainOfArrow>::const_iterator itPROA;
	std::vector<PowerupRainOfArrow> proaArray;

	std::vector<EarthEnemyProjectiles>::const_iterator itenemyproj;
	std::vector<EarthEnemyProjectiles> enemyprojectileArray;

	std::vector<ShooterEnemy>::const_iterator itShooterEnemy;
	std::vector<ShooterEnemy> shootenemyArray;
	
	std::vector<PowerupRapidFire>::const_iterator itPRF;
	std::vector<PowerupRapidFire> prfArray;

	std::vector<PowerupPowerShot>::const_iterator itPS;
	std::vector<PowerupPowerShot> psArray;

	std::vector<PowerupSpeedBoost>::const_iterator itSB;
	std::vector<PowerupSpeedBoost> sbArray;

	std::vector<HazardMud>::const_iterator itMud;
	std::vector<HazardMud> mudArray;

	std::vector<EnemyBossProjectile>::const_iterator itBossProj;
	std::vector<EnemyBossProjectile> bossprojArray;

	std::vector<EarthBossSpear>::const_iterator itBossSpear;
	std::vector<EarthBossSpear> bossSpearArray;

	std::vector<BossCrusherArms>::const_iterator itBossCrush;
	std::vector<BossCrusherArms> bossCrushArray;

	std::vector<BeamerEnemy>::const_iterator itBeamer;
	std::vector<BeamerEnemy> beamerEnemyArray;

	std::vector<BeamProjectile>::const_iterator itBeamProjectile;
	std::vector<BeamProjectile> beamProjectileArray;

	if (!window.isOpen())return-1;
	const float targetTime = 1.0f / 60.f;
	const float TimePerFrame = 1.0f / 16.0f;
	
	float accumalotor = 0.0f;
	float frameTime = 0.0f;
	
	sf::Clock clock;

	while (window.isOpen())
	{
		sf::Time deltaTime = clock.restart();
		frameTime = std::min(deltaTime.asSeconds(), 0.1f);
		accumalotor += frameTime;
		while (accumalotor > targetTime)
		{
			accumalotor -= targetTime;
			sf::Event event;
			while (window.pollEvent(event))
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				{
					window.close();
				}
				else if (event.type == sf::Event::Closed)
				{
					window.close();
				}

			}

		}

		if (ClearArraysOnce)
		{
			playerUnderdel.Bossfightmode = true;
			player.setPosition(480, 1192);
			enemyArray.clear();
			shootenemyArray.clear();
			enemyprojectileArray.clear();
			proaArray.clear();
			prfArray.clear();
			psArray.clear();
			sbArray.clear();
			mudArray.clear();
			beamerEnemyArray.clear();
			beamProjectileArray.clear();
			arrowArray.clear();
			bossCrushArray.clear();
			bossSpearArray.clear();
			bossprojArray.clear();
			EnemyProjectileSpawnCounter = -1;
			GolemSpawnCounter = -1;
			proaSpawnCounter = -1;
			rfSpawnCounter = -1;
			psSpawnCounter = -1;
			sbSpawnCounter = -1;
			ArrowSpawnCounter = -1;
			ShooterEnemySpawnCounter = -1;
			BossProjSpawnCounter = -1;
			BossSpearSpawnCounter = -1;
			BossCrushspawnCounter = -1;
			BeamspawnCounter = -1;
			BeamerSpawnCounter = -1;
			MudSpawnCounter = -1;
			ClearArraysOnce = false;
		}

		if (Menu)
		{
			if (!BackgroundOnce)
			{
				counter = 0;
				counter2 = 0;
				counter3 = 0;
				BackgroundOnce = true;
				MoveBackground = 0;
				MoveBackgroundLeft = 0;
				MoveBackgroundRight = 0;
				IntroAnimationX = 0;
				IntroAnimationY = 9;
				IntroCamaronY = 1068;
			}
			///////////Menu Screen stuff/////////
			{
				CrosshairSprite.setTextureRect(sf::IntRect(CrosshairAnimationX * 32, CrosshairAnimationY * 32, 32, 32));
				sf::Vector2i aimPos = sf::Mouse::getPosition(window);
				CrosshairSprite.setPosition(aimPos.x - CrosshairTexture.getSize().x / 2, aimPos.y - CrosshairTexture.getSize().y / 2);
				cursorRect.setPosition(aimPos.x - CrosshairTexture.getSize().x / 2, aimPos.y - CrosshairTexture.getSize().y / 2);
				backgroundMenuSprite.setPosition(0, 0 + MoveBackground);
				IntroScreen.setPosition(0, 768 + MoveBackground);
				StartButtonSprite.setPosition(100 + MoveBackgroundLeft + MoveBackgroundRight, 250 + MoveBackground);
				StartButtonSpriteIdle.setPosition(100 + MoveBackgroundLeft + MoveBackgroundRight, 250 + MoveBackground);
				OptionsButtonSprite.setPosition(300 + MoveBackgroundLeft + MoveBackgroundRight, 250 + MoveBackground);
				OptionsButtonSpriteIdle.setPosition(300 + MoveBackgroundLeft + MoveBackgroundRight, 250 + MoveBackground);
				ScoreButtonSprite.setPosition(500 + MoveBackgroundLeft + MoveBackgroundRight, 250 + MoveBackground);
				ScoreButtonSpriteIdle.setPosition(500 + MoveBackgroundLeft + MoveBackgroundRight, 250 + MoveBackground);
				QuitButtonSprite.setPosition(700 + MoveBackgroundLeft + MoveBackgroundRight, 250 + MoveBackground);
				QuitButtonSpriteIdle.setPosition(700 + MoveBackgroundLeft + MoveBackgroundRight, 250 + MoveBackground);
				Menurect1.setPosition(135 + MoveBackgroundLeft + MoveBackgroundRight, 285 + MoveBackground);
				Menurect2.setPosition(335 + MoveBackgroundLeft + MoveBackgroundRight, 285 + MoveBackground);
				Menurect3.setPosition(535 + MoveBackgroundLeft + MoveBackgroundRight, 285 + MoveBackground);
				Menurect4.setPosition(735 + MoveBackgroundLeft + MoveBackgroundRight, 285 + MoveBackground);
				EnemiesWalking.setPosition(282, 458 + MoveBackground);
				IntroCamaron.setPosition(484, IntroCamaronY);
				MainMenuText.setPosition(222 + MoveBackgroundLeft + MoveBackgroundRight, 100 + MoveBackground);
				MainMenuScreenGlowing.setPosition(222 + MoveBackgroundLeft + MoveBackgroundRight, 100 + MoveBackground);
				OptionsText.setPosition(1024 + 353 + MoveBackgroundLeft + MoveBackgroundRight, 70 + MoveBackground);
				OptionsSettings.setPosition(1024 + 313 + MoveBackgroundLeft + MoveBackgroundRight, 170 + MoveBackground);
				BackArrowOptions.setPosition(1064 + MoveBackgroundLeft + MoveBackgroundRight, 40);
				BackArrowOptionsSelected.setPosition(1064 + MoveBackgroundLeft + MoveBackgroundRight, 40);
				Menurect5.setPosition(1064 + MoveBackgroundLeft + MoveBackgroundRight, 40);
				Menurect6.setPosition(1588 + MoveBackgroundLeft + MoveBackgroundRight, 161);
				Menurect7.setPosition(1588 + MoveBackgroundLeft + MoveBackgroundRight, 209);
				FullscreenCross.setPosition(1625 + MoveBackgroundLeft + MoveBackgroundRight, 231);
				SoundCross.setPosition(1625 + MoveBackgroundLeft + MoveBackgroundRight, 183);
				
			}
			
			///////Setting Scale//////
			{
				SoundCross.setScale(0.99, 0.99);
				FullscreenCross.setScale(0.99, 0.99);
				/*IntroScreen.setScale(0.75, 0.75);
				StartButtonSprite.setScale(0.75, 0.75);
				StartButtonSpriteIdle.setScale(0.75, 0.75);
				OptionsButtonSprite.setScale(0.75, 0.75);
				OptionsButtonSpriteIdle.setScale(0.75, 0.75);
				ScoreButtonSprite.setScale(0.75, 0.75);
				ScoreButtonSpriteIdle.setScale(0.75, 0.75);
				QuitButtonSprite.setScale(0.75, 0.75);
				QuitButtonSpriteIdle.setScale(0.75, 0.75);*/
				OptionsSettings.setScale(1.5, 1.5);
			}

			///////WalkingAnimation for Enemies///////
			{
				if (IntroEnemyAnimationCounter == 10)
				{
					IntroEnemyAnimationY++;
					IntroEnemyAnimationCounter = 0;
				}
				IntroEnemyAnimationCounter++;

				if (IntroEnemyAnimationY * 64 > 9 * 64)
				{
					IntroEnemyAnimationY = 0;
				}
				EnemiesWalking.setTextureRect(sf::IntRect(0, IntroEnemyAnimationY * 48, 460, 48));

			}
			
			



			/////////Drawing stuff/////////
			{
				window.draw(backgroundMenuSprite);
				window.draw(MainMenuText);
				
				window.draw(OptionsText);
				window.draw(OptionsSettings);
				window.draw(IntroScreen);
				window.draw(EnemiesWalking);
			}
			
			if (IntroplayerActivate)
			{
				if (IntroAnimationCounter == 4)
				{
					IntroAnimationX++;
					
					IntroAnimationCounter = 0;
				}
				if (IntroAnimationX * 64 > 7 * 64)
				{
					IntroAnimationX = 0;
				}
				IntroCamaron.setTextureRect(sf::IntRect(IntroAnimationX * 64, IntroAnimationY * 64, 64, 64));
				IntroAnimationCounter++;
				IntroCamaronY -= 5;

				window.draw(IntroCamaron);

				if (IntroCamaron.getPosition().y <= 600)
				{
					Menu = false;
					Playstate = true;
				}
			}


			if (cursorRect.getGlobalBounds().intersects(Menurect1.getGlobalBounds()))
			{
				if (PlayMusic)
					Menusound.play();
				
				window.draw(StartButtonSprite);
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					PrePlaystate = true;
				}

			}
			else
				window.draw(StartButtonSpriteIdle);

			if (cursorRect.getGlobalBounds().intersects(Menurect5.getGlobalBounds()))
			{
				window.draw(BackArrowOptionsSelected);
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					MovebackFromRight = true;
				}

			}
			else
				window.draw(BackArrowOptions);

			if (cursorRect.getGlobalBounds().intersects(Menurect2.getGlobalBounds()))
			{
				if (PlayMusic)
					Menusound.play();

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					Options = true;
					//BackgroundOnce = true;
				}
				window.draw(OptionsButtonSprite);

			}
			else
				window.draw(OptionsButtonSpriteIdle);

			if (cursorRect.getGlobalBounds().intersects(Menurect3.getGlobalBounds()))
			{
				if (PlayMusic)
					Menusound.play();

				window.draw(ScoreButtonSprite);

			}
			else
				window.draw(ScoreButtonSpriteIdle);

			if (cursorRect.getGlobalBounds().intersects(Menurect4.getGlobalBounds()))
			{
				if (PlayMusic)
					Menusound.play();

				window.draw(QuitButtonSprite);

			}
			else
				window.draw(QuitButtonSpriteIdle);

			if (cursorRect.getGlobalBounds().intersects(Menurect6.getGlobalBounds()))
			{
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (!PlayMusic)
					{
						PlayMusic = true;
					}
					else
					{
						PlayMusic = false;
					}

				}
			}


			if (cursorRect.getGlobalBounds().intersects(Menurect7.getGlobalBounds()))
			{
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (!FullscreenActive)
					{
						FullscreenActive = true;
						window.create(sf::VideoMode(SCREENWIDTH, SCREENHEIGHT), "Agtoschmemp", sf::Style::Fullscreen | sf::Style::Close);
						window.setFramerateLimit(60);
						window.setMouseCursorVisible(false);
					}
					else
					{
						FullscreenActive = false;
						window.create(sf::VideoMode(SCREENWIDTH, SCREENHEIGHT), "Agtoschmemp", sf::Style::Resize | sf::Style::Close);
						window.setFramerateLimit(60);
						window.setMouseCursorVisible(false);
					}
					
				}
			}
			
			if (FullscreenActive)
			{
				window.draw(FullscreenCross);
			}

			if (PlayMusic)
			{
				window.draw(SoundCross);
			}


			window.draw(CrosshairSprite);

			if (Options)
			{
				if (counter2 <= 65)
				{
					if (counter2 <= 64)
					{
						MoveBackgroundLeft -= 16;
					}
					else 
					{
						Options = false;
					}
					
					
				}
				counter2++;
			}
			
			if (MovebackFromRight)
			{
				if (counter3 <= 65)
				{
					if (counter3 <= 64)
					{
						MoveBackgroundLeft += 16;
					}
					else
					{
						MovebackFromRight = false;
						BackgroundOnce = false;
					}
					

				}
				counter3++;
			}

			if (PrePlaystate)
			{
				if (counter <= 159)
				{
					if (counter <= 64)
					{
						MoveBackground -= 8;
					}
					else if (counter <= 110)
					{
						MoveBackground -= 4;
					}
					else if (counter <= 126)
					{
						MoveBackground -= 2;
					}
					else if (counter <= 158)
					{
						MoveBackground -= 1;
					}
					else if (counter <= 159)
					{
						IntroplayerActivate = true;
					}
				}
				counter++;
			}

			
			window.display();
		}
		else if (Playstate)
		{
			
			if(DistanceCounter == 60)
			{
				distanceRunning += (5 + SpeedBoostPower + SpeedBoostSlow);
				std::cout << distanceRunning << std::endl;
				DistanceCounter = 0;
			}
			DistanceCounter++;

			if (AnimationCounter == 4)
			{

				AnimationsX++;
				AnimationCounter = 0;

			}
			AnimationCounter++;

			if (AnimationsX * 64 >= 14 * 64)
			{
				AnimationsX = 0;
			}

			if (PlayerAnimation.y == 0)
			{
				if (PlayerAnimation.x * 64 >= PlayerTexture.getSize().x)
				{
					PlayerAnimation.x = 0;
					PlayerAnimation.y = 1;
				}
			}
			else if (PlayerAnimation.y == 1)
			{
				if (PlayerAnimation.x * 64 >= PlayerTexture.getSize().x - 2 * 64)
				{
					PlayerAnimation.x = 0;
					PlayerAnimation.y = 0;
				}
			}


			if (CrosshairAnimationbool)
			{
				if (rapidFireCooldown == 60)
				{
					if (CrosshairAnimationCounter == 4)
					{
						
						CrosshairAnimationX++;
						CrosshairAnimationCounter = 0;
					}
					CrosshairAnimationCounter++;
				}
				else
				{
					if (CrosshairAnimationCounter == 2)
					{
						
						CrosshairAnimationX++;
						CrosshairAnimationCounter = 0;
					}
					CrosshairAnimationCounter++;
				}
			}
			
			if (CrosshairAnimationX == 15)
			{
				CrosshairAnimationbool = false;
				CrosshairAnimationX = 0;
			}

			
			player.updateMovement();
			HandleEvents();

			

			CrosshairSprite.setTextureRect(sf::IntRect(CrosshairAnimationX * 32, CrosshairAnimationY * 32, 32, 32));

			sf::Vector2i aimPos = sf::Mouse::getPosition(window);
			CrosshairSprite.setPosition(aimPos.x - 16, aimPos.y - 16);
			if (CrosshairSprite.getPosition().y + 16 > player.sprite.getPosition().y + 32)
			{
				CrosshairAnimationY = 1;
			}
			else
			{
				CrosshairAnimationY = 0;
			}

			///////////////// UPDATE ////////////////////
			{
				hudFace.GetFaceCounter(hourglass.ReturnFaceCounter());
				hudFace.update();
				playerUnderdel.SetPlayerPos(player.sprite.getPosition().x, player.sprite.getPosition().y);
				playerUnderdel.update();
				if (Activateboss)
				{
					playerUnderdel.HandleEvents();
				}
				HUDPowerupSprite.setScale(3, 3);
				hourglass.GetHealth(player.returnHealth());
				hourglass.update();
				player.update();
				
				HUDPowerupSprite.setPosition(SCREENWIDTH - 37.5 * HUDPowerupSprite.getScale().x, SCREENHEIGHT - 54.5 * HUDPowerupSprite.getScale().y);

				if (WhatPowerup == 0)
				{
					HUDPowerupSprite.setTextureRect(sf::IntRect(AnimationsX * 32, WhatPowerup * 64, 32, 64));
				}
				else if (WhatPowerup == 1)
				{
					HUDPowerupSprite.setTextureRect(sf::IntRect(AnimationsX * 32, WhatPowerup * 64, 32, 64));
				}
				else if (WhatPowerup == 2)
				{
					HUDPowerupSprite.setTextureRect(sf::IntRect(AnimationsX * 32, WhatPowerup * 64, 32, 64));
				}
				else if (WhatPowerup == 3)
				{
					HUDPowerupSprite.setTextureRect(sf::IntRect(AnimationsX * 32, WhatPowerup * 64, 32, 64));
				}
				else
				{
					HUDPowerupSprite.setTextureRect(sf::IntRect(AnimationsX * 32, 4 * 64, 32, 64));
				}

				if (player.returnHealth() == 6)
				{
					//window.close();
				}
			}
			/////////////////////////////////////////////

			////////////////Collision////////////////////

			//Arrow + Enemy//
			{
				counter = ArrowSpawnCounter;
				for (itArrow = arrowArray.end(); itArrow != arrowArray.begin(); itArrow--)
				{
					if (arrowArray[counter].CheckHit())
					{
						counter2 = GolemSpawnCounter;
						for (itEnemy = enemyArray.end(); itEnemy != enemyArray.begin(); itEnemy--)
						{
							if (enemyArray[counter2].CheckHit())
							{
								if (arrowArray[counter].rect.getGlobalBounds().intersects(enemyArray[counter2].rect.getGlobalBounds()))
								{
									if (PlayMusic)
										Deathsound.play();

									if (!PowerShotCooldown)
										arrowArray[counter].SetVisible();

									enemyArray[counter2].SetVisible();
									Score++;
									Poweruprandomiser = rand() + 1;
									if (Poweruprandomiser % 20 <= 5 &&Poweruprandomiser % 20 > 0)
									{
										Poweruprandomiser %= 20;
										if (Poweruprandomiser == 1)
										{
											rainofArrow.SetPosition(enemyArray[counter2].sprite.getPosition().x + 32, enemyArray[counter2].sprite.getPosition().y + 32);
											rainofArrow.SetSpeedBoost(SpeedBoostPower);
											proaArray.push_back(rainofArrow);
											proaSpawnCounter++;
										}
										if (Poweruprandomiser == 2)
										{
											rapidFire.SetPosition(enemyArray[counter2].sprite.getPosition().x + 32, enemyArray[counter2].sprite.getPosition().y + 32);
											rapidFire.SetSpeedBoost(SpeedBoostPower);
											prfArray.push_back(rapidFire);
											rfSpawnCounter++;
										}
										if (Poweruprandomiser == 3)
										{
											powerShot.SetPosition(enemyArray[counter2].sprite.getPosition().x + 32, enemyArray[counter2].sprite.getPosition().y + 32);
											powerShot.SetSpeedBoost(SpeedBoostPower);
											psArray.push_back(powerShot);
											psSpawnCounter++;
										}
										if (Poweruprandomiser == 4)
										{
											speedBoost.SetPosition(enemyArray[counter2].sprite.getPosition().x + 32, enemyArray[counter2].sprite.getPosition().y + 32);
											speedBoost.SetSpeedBoost(SpeedBoostPower);
											sbArray.push_back(speedBoost);
											sbSpawnCounter++;
										}
										if (Poweruprandomiser == 5)
										{
											int MudPos;
											MudPos = rand();
											MudPos %= 700;
											if (MudPos > 200 && MudPos < 700)
											{
												int m = 0;
												int n = 0;
												for (int i = 0; i < 9; i++)
												{
													mudHazard.SetSpeedBoost(SpeedBoostPower);
													mudHazard.SetPos(MudPos + 64 * m, -200 + 64 * n);
													mudHazard.SetTextureRect(m, n);
													mudArray.push_back(mudHazard);
													MudSpawnCounter++;
													m++;
													if (m == 3)
													{
														n++;
														m = 0;
													}
												}
											}

										}
									}
								}
							}
							counter2--;
						}
					}
					counter--;
				}
			}

			// Arrow + ShootEnemy
			{
				counter = ArrowSpawnCounter;
				for (itArrow = arrowArray.end(); itArrow != arrowArray.begin(); itArrow--)
				{
					if (arrowArray[counter].CheckHit())
					{
						counter2 = ShooterEnemySpawnCounter;
						for (itShooterEnemy = shootenemyArray.end(); itShooterEnemy != shootenemyArray.begin(); itShooterEnemy--)
						{
							if (shootenemyArray[counter2].CheckHit())
							{
								if (arrowArray[counter].rect.getGlobalBounds().intersects(shootenemyArray[counter2].rect.getGlobalBounds()))
								{
									if (PlayMusic)
										Deathsound.play();

									if (!PowerShotCooldown)
										arrowArray[counter].SetVisible();

									shootenemyArray[counter2].SetVisible();
									Score++;
									Poweruprandomiser = rand() + 1;

									if (Poweruprandomiser % 20 <= 5 && Poweruprandomiser % 20 > 0)
									{
										Poweruprandomiser %= 20;
										if (Poweruprandomiser == 1)
										{
											rainofArrow.SetPosition(shootenemyArray[counter2].sprite.getPosition().x + 32, shootenemyArray[counter2].sprite.getPosition().y + 32);
											rainofArrow.SetSpeedBoost(SpeedBoostPower);
											proaArray.push_back(rainofArrow);
											proaSpawnCounter++;
										}
										if (Poweruprandomiser == 2)
										{
											rapidFire.SetPosition(shootenemyArray[counter2].sprite.getPosition().x + 32, shootenemyArray[counter2].sprite.getPosition().y + 32);
											rapidFire.SetSpeedBoost(SpeedBoostPower);
											prfArray.push_back(rapidFire);
											rfSpawnCounter++;
										}
										if (Poweruprandomiser == 3)
										{
											powerShot.SetPosition(shootenemyArray[counter2].sprite.getPosition().x + 32, shootenemyArray[counter2].sprite.getPosition().y + 32);
											powerShot.SetSpeedBoost(SpeedBoostPower);
											psArray.push_back(powerShot);
											psSpawnCounter++;
										}
										if (Poweruprandomiser == 4)
										{
											speedBoost.SetPosition(shootenemyArray[counter2].sprite.getPosition().x + 32, shootenemyArray[counter2].sprite.getPosition().y + 32);
											speedBoost.SetSpeedBoost(SpeedBoostPower);
											sbArray.push_back(speedBoost);
											sbSpawnCounter++;
										}
										if (Poweruprandomiser == 5)
										{
											int MudPos;
											MudPos = rand();
											MudPos %= 700;
											if (MudPos > 200 && MudPos < 700)
											{
												int m = 0;
												int n = 0;
												for (int i = 0; i < 9; i++)
												{
													mudHazard.SetSpeedBoost(SpeedBoostPower);
													mudHazard.SetPos(MudPos + 64 * m, -200 + 64 * n);
													mudHazard.SetTextureRect(m, n);
													mudArray.push_back(mudHazard);
													MudSpawnCounter++;
													m++;
													if (m == 3)
													{
														n++;
														m = 0;
													}
												}
											}

										}
										
									}
								}
							}
							counter2--;
						}
					}
					counter--;
				}
			}

			//Player + Enemy//
			{
				counter = GolemSpawnCounter;
				for (itEnemy = enemyArray.end(); itEnemy != enemyArray.begin(); itEnemy--)
				{
					if (enemyArray[counter].CheckHit())
					{
						if (enemyArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
						{
							if (PlayMusic)
								Deathsound.play();

							player.setHealth();
							player.Damaged();
							playerUnderdel.Damaged();
						}
					}
					counter--;
				}
			}

			//Player + ShootEnemy//
			{
				counter = ShooterEnemySpawnCounter;
				for (itShooterEnemy = shootenemyArray.end(); itShooterEnemy != shootenemyArray.begin(); itShooterEnemy--)
				{
					if (shootenemyArray[counter].CheckHit())
					{
						if (shootenemyArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
						{
							if (PlayMusic)
								Deathsound.play();

							player.setHealth();
							player.Damaged();
							playerUnderdel.Damaged();
						}
						counter--;
					}
				}
			}

			//Player + EnemyProjectile//
			{
				counter = EnemyProjectileSpawnCounter;
				for (itenemyproj = enemyprojectileArray.end(); itenemyproj != enemyprojectileArray.begin(); itenemyproj--)
				{
					if (enemyprojectileArray[counter].CheckHit())
					{
						if (enemyprojectileArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
						{
							player.setHealth();
							player.Damaged();
							playerUnderdel.Damaged();
						}
						counter--;
					}
				}
			}

			//Player + Wall//
			{
				if (player.rect.getGlobalBounds().intersects(Leftwall.getGlobalBounds()))
				{
					player.setPosition(player.sprite.getPosition().x + (player.sprite.getPosition().x - Leftwall.getPosition().x + 4), player.sprite.getPosition().y);
				}
				if (player.rect.getGlobalBounds().intersects(Rightwall.getGlobalBounds()))
				{
					player.setPosition(player.sprite.getPosition().x + (Rightwall.getPosition().x - (player.sprite.getPosition().x + 43)), player.sprite.getPosition().y);
				}

			}

			//Player + Rain of Arrows Powerup//
			{
				counter = proaSpawnCounter;
				for (itPROA = proaArray.end(); itPROA != proaArray.begin(); itPROA--)
				{
					if (proaArray[counter].CheckHit())
					{
						if (proaArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
						{
							proaArray[counter].SetVisible();
							Score++;
							WhatPowerup = 1;
						}
					}
					counter--;
				}

			}

			//Player + Rapid Fire//
			{
				counter = rfSpawnCounter;
				for (itPRF = prfArray.end(); itPRF != prfArray.begin(); itPRF--)
				{
					if (prfArray[counter].CheckHit())
					{
						if (prfArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
						{
							prfArray[counter].SetVisible();
							Score++;
							WhatPowerup = 2;
						}
					}
					counter--;
				}
			}

			//Player + Power Shot//
			{
				counter = psSpawnCounter;
				for (itPS = psArray.end(); itPS != psArray.begin(); itPS--)
				{
					if (psArray[counter].CheckHit())
					{
						if (psArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
						{
							psArray[counter].SetVisible();
							Score++;
							WhatPowerup = 0;
						}
					}
					counter--;
				}
			}

			//Player + Speed Boost//
			{
				counter = sbSpawnCounter;
				for (itSB = sbArray.end(); itSB != sbArray.begin(); itSB--)
				{
					if (sbArray[counter].CheckHit())
					{
						if (sbArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
						{
							sbArray[counter].SetVisible();
							Score++;
							WhatPowerup = 3;
						}
					}
					counter--;
				}
			}

			//Player + Mud//
			{
				counter = MudSpawnCounter;
				for (itMud = mudArray.end(); itMud != mudArray.begin(); itMud--)
				{
					if (mudArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
					{
						mudArray[counter].SetSlowed();
					}
					counter--;
				}

				counter = MudSpawnCounter;
				for (itMud = mudArray.end(); itMud != mudArray.begin(); itMud--)
				{
					if (mudArray[counter].GetSlowed())
					{
						if (mudArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
						{
							mudArray[counter].SetSlowed();
						}
						else
						{
							mudArray[counter].SetSlowedFalse();
						}

						if (mudArray[counter].GetSlowed())
						{
							SpeedBoostSlow = 2;
						}
						else
						{
							SpeedBoostSlow = 0;
						}
					}
					counter--;
				}
			}

			//Player + BossProjectile//
			{
				counter = BossProjSpawnCounter;
				for (itBossProj = bossprojArray.end(); itBossProj != bossprojArray.begin(); itBossProj--)
				{
					if (bossprojArray[counter].CheckHit())
					{
						if (bossprojArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
						{
							
							if (PlayMusic)
								Deathsound.play();
							player.setHealth();
							player.Damaged();
							playerUnderdel.Damaged();
						}
					}

					counter--;
				}
			}

			//Boss + Arrow//
			{
				if (Activateboss)
				{
					counter = ArrowSpawnCounter;
					for (itArrow = arrowArray.end(); itArrow != arrowArray.begin(); itArrow--)
					{
						if (arrowArray[counter].CheckHit())
						{
							if (arrowArray[counter].rect.getGlobalBounds().intersects(earthBoss.rect.getGlobalBounds()))
							{
								arrowArray[counter].SetVisible();
								earthBoss.SetVisible();
							}
						}
						counter--;
					}
				}
				
			}

			//Player + Spear//
			{
				counter = BossSpearSpawnCounter;
				for (itBossSpear = bossSpearArray.end(); itBossSpear != bossSpearArray.begin(); itBossSpear--)
				{
					if (bossSpearArray[counter].IsActive())
					{
						if (bossSpearArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
						{
							player.setHealth();
							player.Damaged();
							playerUnderdel.Damaged();
						}
					}
					counter--;
				}

			}

			//Player + Crusher//
			{
				counter = BossCrushspawnCounter;
				for (itBossCrush = bossCrushArray.end(); itBossCrush != bossCrushArray.begin(); itBossCrush--)
				{
					if (bossCrushArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
					{
						player.setHealth();
						player.Damaged();
						playerUnderdel.Damaged();
					}
					counter--;
				}
			}

			//Arrow + Beamer//
			{
				counter = BeamerSpawnCounter;
				for (itBeamer = beamerEnemyArray.end(); itBeamer != beamerEnemyArray.begin(); itBeamer--)
				{
					if (beamerEnemyArray[counter].CheckHit())
					{
						counter2 = ArrowSpawnCounter;
						for (itArrow = arrowArray.end(); itArrow != arrowArray.begin(); itArrow--)
						{
							if (arrowArray[counter2].CheckHit())
							{
								if (beamerEnemyArray[counter].rect.getGlobalBounds().intersects(arrowArray[counter2].rect.getGlobalBounds()))
								{

									beamerEnemyArray[counter].SetVisible();
									arrowArray[counter2].SetVisible();


									Score++;
									Poweruprandomiser = rand() + 1;
									if (Poweruprandomiser % 20 <= 5 && Poweruprandomiser % 20 > 0)
									{
										Poweruprandomiser %= 20;
										if (Poweruprandomiser == 1)
										{
											rainofArrow.SetPosition(beamerEnemyArray[counter].sprite.getPosition().x + 32, beamerEnemyArray[counter].sprite.getPosition().y + 32);
											rainofArrow.SetSpeedBoost(SpeedBoostPower);
											proaArray.push_back(rainofArrow);
											proaSpawnCounter++;
										}
										if (Poweruprandomiser == 2)
										{
											rapidFire.SetPosition(beamerEnemyArray[counter].sprite.getPosition().x + 32, beamerEnemyArray[counter].sprite.getPosition().y + 32);
											rapidFire.SetSpeedBoost(SpeedBoostPower);
											prfArray.push_back(rapidFire);
											rfSpawnCounter++;
										}
										if (Poweruprandomiser == 3)
										{
											powerShot.SetPosition(beamerEnemyArray[counter].sprite.getPosition().x + 32, beamerEnemyArray[counter].sprite.getPosition().y + 32);
											powerShot.SetSpeedBoost(SpeedBoostPower);
											psArray.push_back(powerShot);
											psSpawnCounter++;
										}
										if (Poweruprandomiser == 4)
										{
											speedBoost.SetPosition(beamerEnemyArray[counter].sprite.getPosition().x + 32, beamerEnemyArray[counter].sprite.getPosition().y + 32);
											speedBoost.SetSpeedBoost(SpeedBoostPower);
											sbArray.push_back(speedBoost);
											sbSpawnCounter++;
										}
										if (Poweruprandomiser == 5)
										{
											int MudPos;
											MudPos = rand();
											MudPos %= 700;
											if (MudPos > 200 && MudPos < 700)
											{
												int m = 0;
												int n = 0;
												for (int i = 0; i < 9; i++)
												{
													mudHazard.SetSpeedBoost(SpeedBoostPower);
													mudHazard.SetPos(MudPos + 64 * m, -200 + 64 * n);
													mudHazard.SetTextureRect(m, n);
													mudArray.push_back(mudHazard);
													MudSpawnCounter++;

													m++;
													if (m == 3)
													{
														n++;
														m = 0;
													}
												}
											}

										}
									}
								}

							}
							counter2--;
						}
					}
					counter--;
				}
			}

			//Player + BeamerProj//
			{
				counter = BeamspawnCounter;
				for (itBeamProjectile = beamProjectileArray.end(); itBeamProjectile != beamProjectileArray.begin(); itBeamProjectile--)
				{
					if (beamProjectileArray[counter].CheckHit())
					{
						if (beamProjectileArray[counter].rect.getGlobalBounds().intersects(player.rect.getGlobalBounds()))
						{
							player.setHealth();
							player.Damaged();
							playerUnderdel.Damaged();
						}
					}
					counter--;
				}
			}


			/////////////////////////////////////////////

			//Powerup stuff//
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
				{
					if (player.resetPowerDuration())
					{

						//////////////Powershot/////////////////
						if (WhatPowerup == 0)
						{
							PowerShotCooldown = true;
							WhatPowerup = 4;
						}
						/////////////Rain Of Arrow/////////////
						else if (WhatPowerup == 1)
						{
							counter = GolemSpawnCounter;
							for (itEnemy = enemyArray.end(); itEnemy != enemyArray.begin(); itEnemy--)
							{
								if (enemyArray[counter].CheckHit())
								{
									enemyArray[counter].SetVisible();
									Score++;
								}
								counter--;

							}
							counter = BeamerSpawnCounter;
							for (itBeamer = beamerEnemyArray.end(); itBeamer != beamerEnemyArray.begin(); itBeamer--)
							{
								if (beamerEnemyArray[counter].CheckHit())
								{
									beamerEnemyArray[counter].SetVisible();
									Score++;
								}
								counter--;
							}

							counter = ShooterEnemySpawnCounter;
							for (itShooterEnemy = shootenemyArray.end(); itShooterEnemy != shootenemyArray.begin(); itShooterEnemy--)
							{
								if (shootenemyArray[counter].CheckHit())
								{
									shootenemyArray[counter].SetVisible();
									Score++;
								}
								
								counter--;
							}
							if (PlayMusic)
							{
								RoAsound.play();
							}
							WhatPowerup = 4;
						}
						//////////////Rapid Fire///////////////
						else if (WhatPowerup == 2)
						{
							rapidFireCooldown = 30;
							WhatPowerup = 4;
						}
						//////////////Speed Boost//////////////
						else if (WhatPowerup == 3)
						{
							WhatPowerup = 4;
							SpeedBoostPower = 5;
							
						}
					}
				}
				//////////Reseting powerup//////////////
				if (player.returnPowerDuration() == 0)
				{
					if (rapidFireCooldown == 30)
						rapidFireCooldown = 60;
					if (PowerShotCooldown == true)
						PowerShotCooldown = false;

					if (SpeedBoostPower == 5)
					{
						SpeedBoostPower = 0;
						

					}
				}
			}

			////Setting the speed of everething that is visible////
			{
				counter = GolemSpawnCounter;
				for (itEnemy = enemyArray.end(); itEnemy != enemyArray.begin(); itEnemy--)
				{
					if (enemyArray[counter].CheckHit())
					{
						enemyArray[counter].SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
					}
					counter--;
				}

				counter = ShooterEnemySpawnCounter;
				for (itShooterEnemy = shootenemyArray.end(); itShooterEnemy != shootenemyArray.begin(); itShooterEnemy--)
				{
					if (shootenemyArray[counter].CheckHit())
					{
						shootenemyArray[counter].SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
					}
					counter--;
				}

				counter = EnemyProjectileSpawnCounter;
				for (itenemyproj = enemyprojectileArray.end(); itenemyproj != enemyprojectileArray.begin(); itenemyproj--)
				{
					if (enemyprojectileArray[counter].CheckHit())
					{
						enemyprojectileArray[counter].SetSpeedBoost(SpeedBoostPower);
					}
					counter--;
				}

				counter = proaSpawnCounter;
				for (itPROA = proaArray.end(); itPROA != proaArray.begin(); itPROA--)
				{
					if (proaArray[counter].CheckHit())
					{
						proaArray[counter].SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
					}
					counter--;
				}

				counter = rfSpawnCounter;
				for (itPRF = prfArray.end(); itPRF != prfArray.begin(); itPRF--)
				{
					if (prfArray[counter].CheckHit())
					{
						prfArray[counter].SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
					}
					counter--;
				}

				counter = psSpawnCounter;
				for (itPS = psArray.end(); itPS != psArray.begin(); itPS--)
				{
					if (psArray[counter].CheckHit())
					{
						psArray[counter].SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
					}
					counter--;
				}

				counter = sbSpawnCounter;
				for (itSB = sbArray.end(); itSB != sbArray.begin(); itSB--)
				{
					if (sbArray[counter].CheckHit())
					{
						sbArray[counter].SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
					}
					counter++;
				}
				counter = MudSpawnCounter;
				for (itMud = mudArray.end(); itMud != mudArray.begin(); itMud--)
				{
					mudArray[counter].SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
					counter--;
				}
				counter = BeamerSpawnCounter;
				for (itBeamer = beamerEnemyArray.end(); itBeamer != beamerEnemyArray.begin(); itBeamer--)
				{
					beamerEnemyArray[counter].SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
					counter--;
				}

				counter = BeamspawnCounter;
				for (itBeamProjectile = beamProjectileArray.end(); itBeamProjectile != beamProjectileArray.begin(); itBeamProjectile--)
				{
					beamProjectileArray[counter].SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
					counter--;
				}

			}
				

			window.clear(sf::Color(0x00, 0x00, 0x00, 0xff));

			///////Wallstuff///////
			{
				if (Activateboss)
				{
					Rightwall.setPosition(1024, 0);
					Leftwall.setPosition(-32, 0);
				}
				else
				{
					Rightwall.setPosition(835, 0);
					Leftwall.setPosition(157, 0);
				}
			}

			////////////////Background Stuff///////////////
			{

				///////////////Setting IntRect////////////
				{
					BackgroundSpriteX0Y0.setTextureRect(sf::IntRect(3 * 256, 0 * 256, 256, 256));
					BackgroundSpriteX0Y1.setTextureRect(sf::IntRect(3 * 256, 1 * 256, 256, 256));
					BackgroundSpriteX0Y2.setTextureRect(sf::IntRect(3 * 256, 2 * 256, 256, 256));
					BackgroundSpriteX0Y3.setTextureRect(sf::IntRect(3 * 256, 0 * 256, 256, 256));
					BackgroundSpriteX0Y4.setTextureRect(sf::IntRect(3 * 256, 1 * 256, 256, 256));

					BackgroundSpriteX1Y0.setTextureRect(sf::IntRect(0 * 256, 0 * 256, 256, 256));
					BackgroundSpriteX1Y1.setTextureRect(sf::IntRect(0 * 256, 1 * 256, 256, 256));
					BackgroundSpriteX1Y2.setTextureRect(sf::IntRect(0 * 256, 2 * 256, 256, 256));
					BackgroundSpriteX1Y3.setTextureRect(sf::IntRect(0 * 256, 0 * 256, 256, 256));
					BackgroundSpriteX1Y4.setTextureRect(sf::IntRect(0 * 256, 1 * 256, 256, 256));

					BackgroundSpriteX2Y0.setTextureRect(sf::IntRect(1 * 256, 0 * 256, 256, 256));
					BackgroundSpriteX2Y1.setTextureRect(sf::IntRect(1 * 256, 1 * 256, 256, 256));
					BackgroundSpriteX2Y2.setTextureRect(sf::IntRect(1 * 256, 2 * 256, 256, 256));
					BackgroundSpriteX2Y3.setTextureRect(sf::IntRect(1 * 256, 0 * 256, 256, 256));
					BackgroundSpriteX2Y4.setTextureRect(sf::IntRect(1 * 256, 1 * 256, 256, 256));

					BackgroundSpriteX3Y0.setTextureRect(sf::IntRect(2 * 256, 0 * 256, 256, 256));
					BackgroundSpriteX3Y1.setTextureRect(sf::IntRect(2 * 256, 1 * 256, 256, 256));
					BackgroundSpriteX3Y2.setTextureRect(sf::IntRect(2 * 256, 2 * 256, 256, 256));
					BackgroundSpriteX3Y3.setTextureRect(sf::IntRect(2 * 256, 2 * 256, 256, 256));
					BackgroundSpriteX3Y4.setTextureRect(sf::IntRect(2 * 256, 1 * 256, 256, 256));

					//////Sides//////
					BackgroundSidesSpriteX0Y0.setTextureRect(sf::IntRect(0 * 256, 0 * 256, 256, 256));
					BackgroundSidesSpriteX0Y1.setTextureRect(sf::IntRect(1 * 256, 0 * 256, 256, 256));
					BackgroundSidesSpriteX0Y2.setTextureRect(sf::IntRect(2 * 256, 0 * 256, 256, 256));
					BackgroundSidesSpriteX0Y3.setTextureRect(sf::IntRect(0 * 256, 0 * 256, 256, 256));
					BackgroundSidesSpriteX0Y4.setTextureRect(sf::IntRect(1 * 256, 0 * 256, 256, 256));

					BackgroundSidesSpriteX1Y0.setTextureRect(sf::IntRect(0 * 256, 1 * 256, 256, 256));
					BackgroundSidesSpriteX1Y1.setTextureRect(sf::IntRect(1 * 256, 1 * 256, 256, 256));
					BackgroundSidesSpriteX1Y2.setTextureRect(sf::IntRect(2 * 256, 1 * 256, 256, 256));
					BackgroundSidesSpriteX1Y3.setTextureRect(sf::IntRect(0 * 256, 1 * 256, 256, 256));
					BackgroundSidesSpriteX1Y4.setTextureRect(sf::IntRect(2 * 256, 1 * 256, 256, 256));

				}

				///////////////Resetting the pos//////////////////
				{
					if (BackgroundSpriteX0Y0.getPosition().y > SCREENHEIGHT)
					{
						BackgroundY[0] = BackgroundSpriteX0Y4.getPosition().y - 256;
					}
					if (BackgroundSpriteX0Y1.getPosition().y > SCREENHEIGHT)
					{
						BackgroundY[1] = BackgroundSpriteX0Y0.getPosition().y - 256;
					}
					if (BackgroundSpriteX0Y2.getPosition().y > SCREENHEIGHT)
					{
						BackgroundY[2] = BackgroundSpriteX0Y1.getPosition().y - 256;
					}
					if (BackgroundSpriteX0Y3.getPosition().y > SCREENHEIGHT)
					{
						BackgroundY[3] = BackgroundSpriteX0Y2.getPosition().y - 256;
					}
					if (BackgroundSpriteX0Y4.getPosition().y > SCREENHEIGHT)
					{
						BackgroundY[4] = BackgroundSpriteX0Y3.getPosition().y - 256;
					}

					///////////Sides///////////
					if (BackgroundSidesSpriteX0Y0.getPosition().y > SCREENHEIGHT)
					{
						BackgroundSideY[0] = BackgroundSidesSpriteX0Y4.getPosition().y - 256;
					}
					if (BackgroundSidesSpriteX0Y1.getPosition().y > SCREENHEIGHT)
					{
						BackgroundSideY[1] = BackgroundSidesSpriteX0Y0.getPosition().y - 256;
					}
					if (BackgroundSidesSpriteX0Y2.getPosition().y > SCREENHEIGHT)
					{
						BackgroundSideY[2] = BackgroundSidesSpriteX0Y1.getPosition().y - 256;
					}
					if (BackgroundSidesSpriteX0Y3.getPosition().y > SCREENHEIGHT)
					{
						BackgroundSideY[3] = BackgroundSidesSpriteX0Y2.getPosition().y - 256;
					}
					if (BackgroundSidesSpriteX0Y4.getPosition().y > SCREENHEIGHT)
					{
						BackgroundSideY[4] = BackgroundSidesSpriteX0Y3.getPosition().y - 256;
					}
				}

				//////////////Setting pos//////////////
				{
					for (int i = 0; i < 5; i++)
					{
						BackgroundY[i] += 5 + SpeedBoostPower - SpeedBoostSlow;
					}

					BackgroundSpriteX0Y0.setPosition(BackgroundX[0], BackgroundY[0]);
					BackgroundSpriteX0Y1.setPosition(BackgroundX[0], BackgroundY[1]);
					BackgroundSpriteX0Y2.setPosition(BackgroundX[0], BackgroundY[2]);
					BackgroundSpriteX0Y3.setPosition(BackgroundX[0], BackgroundY[3]);
					BackgroundSpriteX0Y4.setPosition(BackgroundX[0], BackgroundY[4]);

					BackgroundSpriteX1Y0.setPosition(BackgroundX[1], BackgroundY[0]);
					BackgroundSpriteX1Y1.setPosition(BackgroundX[1], BackgroundY[1]);
					BackgroundSpriteX1Y2.setPosition(BackgroundX[1], BackgroundY[2]);
					BackgroundSpriteX1Y3.setPosition(BackgroundX[1], BackgroundY[3]);
					BackgroundSpriteX1Y4.setPosition(BackgroundX[1], BackgroundY[4]);

					BackgroundSpriteX2Y0.setPosition(BackgroundX[2], BackgroundY[0]);
					BackgroundSpriteX2Y1.setPosition(BackgroundX[2], BackgroundY[1]);
					BackgroundSpriteX2Y2.setPosition(BackgroundX[2], BackgroundY[2]);
					BackgroundSpriteX2Y3.setPosition(BackgroundX[2], BackgroundY[3]);
					BackgroundSpriteX2Y4.setPosition(BackgroundX[2], BackgroundY[4]);

					BackgroundSpriteX3Y0.setPosition(BackgroundX[3], BackgroundY[0]);
					BackgroundSpriteX3Y1.setPosition(BackgroundX[3], BackgroundY[1]);
					BackgroundSpriteX3Y2.setPosition(BackgroundX[3], BackgroundY[2]);
					BackgroundSpriteX3Y3.setPosition(BackgroundX[3], BackgroundY[3]);
					BackgroundSpriteX3Y4.setPosition(BackgroundX[3], BackgroundY[4]);


					///////Sides///////
					for (int i = 0; i < 5; i++)
					{
						BackgroundSideY[i] += 6 + SpeedBoostPower - SpeedBoostSlow;
					}

					BackgroundSidesSpriteX0Y0.setPosition(BackgroundSideX[0], BackgroundSideY[0]);
					BackgroundSidesSpriteX0Y1.setPosition(BackgroundSideX[0], BackgroundSideY[1]);
					BackgroundSidesSpriteX0Y2.setPosition(BackgroundSideX[0], BackgroundSideY[2]);
					BackgroundSidesSpriteX0Y3.setPosition(BackgroundSideX[0], BackgroundSideY[3]);
					BackgroundSidesSpriteX0Y4.setPosition(BackgroundSideX[0], BackgroundSideY[4]);

					BackgroundSidesSpriteX1Y0.setPosition(BackgroundSideX[1], BackgroundSideY[0]);
					BackgroundSidesSpriteX1Y1.setPosition(BackgroundSideX[1], BackgroundSideY[1]);
					BackgroundSidesSpriteX1Y2.setPosition(BackgroundSideX[1], BackgroundSideY[2]);
					BackgroundSidesSpriteX1Y3.setPosition(BackgroundSideX[1], BackgroundSideY[3]);
					BackgroundSidesSpriteX1Y4.setPosition(BackgroundSideX[1], BackgroundSideY[4]);

				}

			}

			///////////////////Background Drawing/////////////////
			{{

					if (Activateboss)
					{
						window.draw(BossArenaScreen);
					}
					else
					{


						window.draw(BackgroundSpriteX0Y0);
						window.draw(BackgroundSpriteX0Y1);
						window.draw(BackgroundSpriteX0Y2);
						window.draw(BackgroundSpriteX0Y3);
						window.draw(BackgroundSpriteX0Y4);
						window.draw(BackgroundSpriteX1Y0);
						window.draw(BackgroundSpriteX1Y1);
						window.draw(BackgroundSpriteX1Y2);
						window.draw(BackgroundSpriteX1Y3);
						window.draw(BackgroundSpriteX1Y4);
						window.draw(BackgroundSpriteX2Y0);
						window.draw(BackgroundSpriteX2Y1);
						window.draw(BackgroundSpriteX2Y2);
						window.draw(BackgroundSpriteX2Y3);
						window.draw(BackgroundSpriteX2Y4);
						window.draw(BackgroundSpriteX3Y0);
						window.draw(BackgroundSpriteX3Y1);
						window.draw(BackgroundSpriteX3Y2);
						window.draw(BackgroundSpriteX3Y3);
						window.draw(BackgroundSpriteX3Y4);
					}

			}}

			////////////////Drawing hazard////////////////////
			{
				counter = MudSpawnCounter;
				for (itMud = mudArray.end(); itMud != mudArray.begin(); itMud--)
				{
					mudArray[counter].update();
					window.draw(mudArray[counter].sprite);
					if (DrawHitbox)
					{
						window.draw(mudArray[counter].rect);
					}
					counter--;
				}
			}

			////////Wave Spawning////////////
			{
				if (!Activateboss)
				{
					if (distanceRunning >= SpawnCounter * 20)
					{
						if (SpawnArray[SpawnCounter - 1] == 1)
						{
							enemy.SetPos(LeftSpawn, SpawnPosition[0]);
							enemy.SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
							enemyArray.push_back(enemy);
							golemCounter = 0;
							GolemSpawnCounter++;
						}

						if (SpawnArray[SpawnCounter - 1] == 2)
						{
							shootenemy.SetPos(LeftSpawn, SpawnPosition[0]);
							shootenemy.SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
							shootenemyArray.push_back(shootenemy);
							ShooterEnemyCounter = 0;
							ShooterEnemySpawnCounter++;
						}
						if (SpawnArray[SpawnCounter - 1] == 3)
						{
							int m = 0;
							for (int i = 0; i < 10; i++)
							{

								shootenemy.SetPos(LeftSpawn + 64 * i, SpawnPosition[m]);
								shootenemy.SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
								shootenemyArray.push_back(shootenemy);
								ShooterEnemyCounter = 0;
								ShooterEnemySpawnCounter++;
								m++;
								if (m == 3)
									m = 0;

							}
						}

						if (SpawnArray[SpawnCounter - 1] == 4)
						{
							beamerEnemy.SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
							beamerEnemy.SetPos(MiddleSpawn, SpawnPosition[1]);
							beamerEnemyArray.push_back(beamerEnemy);
							BeamerSpawnCounter++;
						}
						if (SpawnArray[SpawnCounter - 1] == 5)
						{
							rainofArrow.SetPosition(400, 200);
							rainofArrow.SetSpeedBoost(SpeedBoostPower);
							proaArray.push_back(rainofArrow);
							proaSpawnCounter++;
						}
						if (SpawnArray[SpawnCounter - 1] == 6)
						{
							rapidFire.SetPosition(400, 200);
							rapidFire.SetSpeedBoost(SpeedBoostPower);
							prfArray.push_back(rapidFire);
							rfSpawnCounter++;
						}
						if (SpawnArray[SpawnCounter - 1] == 7)
						{
							powerShot.SetPosition(400, 200);
							powerShot.SetSpeedBoost(SpeedBoostPower);
							psArray.push_back(powerShot);
							psSpawnCounter++;
						}
						if (SpawnArray[SpawnCounter - 1] == 8)
						{
							speedBoost.SetPosition(400, 200);
							speedBoost.SetSpeedBoost(SpeedBoostPower);
							sbArray.push_back(speedBoost);
							sbSpawnCounter++;
						}
						if (SpawnArray[SpawnCounter - 1] == 9)
						{
							PreBossFight = true;
						}
						if (SpawnArray[SpawnCounter - 1] == 10)
						{
							enemy.SetPos(LeftSpawn, SpawnPosition[0]);
							enemy.SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
							enemyArray.push_back(enemy);

							GolemSpawnCounter++;

							enemy.SetPos(RightSpawn, SpawnPosition[0]);
							enemy.SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
							enemyArray.push_back(enemy);

							GolemSpawnCounter++;
						}
						SpawnCounter++;
					}
				}
				
			}
			
			/////////////Player Shooting/////////////
			{
				if (arrowCounter >= rapidFireCooldown)
				{
					if (playerFire)
					{
						
						sf::Vector2i mousePosition = sf::Mouse::getPosition(window);

						const float PI = 3.14159265;
						dx = (aimPos.x + 16 + 16) - (player.sprite.getPosition().x + 32 + 16);
						dy = (aimPos.y + 16 / 2 + 21) - (player.sprite.getPosition().y + 32 + 21);
						Rotation = (atan2(dy, dx)) * 180 / PI;

						if (Rotation < 0)
						{
							CrosshairAnimationbool = true;
							player.Attacking();
							playerUnderdel.Attacking();
							arrowCounter = 0;
						}
						
						
					}
				}
				arrowCounter++;

				playerFire = false;
			}

			////////////OtherPlayer Bullshit//////////
			{
				if (player.ReadyToFire == true)
				{
					if (ArrowcooldownCounter >= 4)
					{
						if (Rotation < 0)
						{

							sf::Vector2i mousePosition = sf::Mouse::getPosition(window);


							ArrowProjectile.fire(mousePosition.y, mousePosition.x, player.sprite.getPosition().y, player.sprite.getPosition().x);
							ArrowProjectile.sprite.setOrigin(16, 21);
							ArrowProjectile.rect.setOrigin(ArrowProjectile.rect.getSize().x / 2, ArrowProjectile.rect.getSize().y / 2);
							ArrowProjectile.rect.setRotation(Rotation + 90);
							ArrowProjectile.sprite.setRotation(Rotation + 90);
							arrowArray.push_back(ArrowProjectile);
							ArrowSpawnCounter++;

						}
						ArrowcooldownCounter = 0;
					}
					ArrowcooldownCounter++;
				}
			}
			
			//////////////Arrow Drawing/////////////
			{
				counter = ArrowSpawnCounter;
				for (itArrow = arrowArray.end(); itArrow != arrowArray.begin(); itArrow--)
				{
					arrowArray[counter].update();
					if (arrowArray[counter].CheckHit())
					{
						window.draw(arrowArray[counter].sprite);
						if (DrawHitbox)
						{
							window.draw(arrowArray[counter].rect);
						}
					}
					counter--;
				}
			}

			//////////////Boss Attacking/////////////////
			{
				if (Activateboss)
				{


					if (BossAttackDelay == 4)
					{
						if (earthBoss.ReadytoFire())
						{
							bossProjectile.fire(earthBoss.sprite.getPosition().x + 10, earthBoss.sprite.getPosition().y + 40, player.sprite.getPosition().x, player.sprite.getPosition().y);
							bossprojArray.push_back(bossProjectile);
							BossProjSpawnCounter++;

							bossProjectile.fire(earthBoss.sprite.getPosition().x + 10, earthBoss.sprite.getPosition().y + 40, player.sprite.getPosition().x + 175, player.sprite.getPosition().y);
							bossprojArray.push_back(bossProjectile);
							BossProjSpawnCounter++;

							bossProjectile.fire(earthBoss.sprite.getPosition().x + 10, earthBoss.sprite.getPosition().y + 40, player.sprite.getPosition().x - 175, player.sprite.getPosition().y);
							bossprojArray.push_back(bossProjectile);
							BossProjSpawnCounter++;

						}

						if (earthBoss.ReadytoImpale())
						{
							int Random = randomChecker.ReturnRandom1to4();

							if (Random == 4)
							{
								LineAttackbackwards = true;

							}
							if (Random == 1)
							{
								
							}
							if (Random == 2)
							{
								LineAttackForwards = true;
							}
							if (Random == 3)
							{
								bossSpear.Shoot(player.sprite.getPosition().x, player.sprite.getPosition().y + 64);
								bossSpearArray.push_back(bossSpear);
								BossSpearSpawnCounter++;
							}


						}

						if (earthBoss.ReadytoCrush())
						{
							bossCrush.Shoot(0 - 96 * bossCrush.sprite.getScale().x, player.sprite.getPosition().y + 32, 2);
							bossCrushArray.push_back(bossCrush);
							BossCrushspawnCounter++;



							bossCrush.Shoot(1024, player.sprite.getPosition().y + 32, 0);
							bossCrushArray.push_back(bossCrush);
							BossCrushspawnCounter++;
						}
						BossAttackDelay = 0;
					}
					BossAttackDelay++;


					if (LineAttackbackwards)
					{
						if (LineAttackCounter2Backwards == 6)
						{
							for (int i = 0; i < 16; i++)
							{
								bossSpear.Shoot(0 + 64 * i, 704 - 64 * LineAttackCounterBackwards);
								bossSpearArray.push_back(bossSpear);
								BossSpearSpawnCounter++;

							}
							LineAttackCounterBackwards++;
							LineAttackCounter2Backwards = 0;
							if (LineAttackCounterBackwards == 5)
							{
								LineAttackbackwards = false;
								LineAttackCounterBackwards = 0;
							}
						}
						LineAttackCounter2Backwards++;

					}

					if (LineAttackForwards)
					{
						if (LineAttackCounter2Forwards == 6)
						{
							for (int i = 0; i < 16; i++)
							{
								bossSpear.Shoot(0 + 64 * i, 300 + 64 * LineAttackCounterForwards);
								bossSpearArray.push_back(bossSpear);
								BossSpearSpawnCounter++;

							}
							LineAttackCounterForwards++;
							LineAttackCounter2Forwards = 0;
							if (LineAttackCounterForwards == 5)
							{
								LineAttackForwards = false;
								LineAttackCounterForwards = 0;
							}
						}
						LineAttackCounter2Forwards++;

					}
				}
			}

			///////////Enemy shooting/////////////
			{
				counter = ShooterEnemySpawnCounter;
				for (itShooterEnemy = shootenemyArray.end(); itShooterEnemy != shootenemyArray.begin(); itShooterEnemy--)
				{
				
					if (shootenemyArray[counter].CheckHit())
					{
						if (shootenemyArray[counter].CheckFire())
						{
							const float PI = 3.14159265;
							dx = (shootenemyArray[counter].sprite.getPosition().x + 64) - (player.sprite.getPosition().x + 32);
							dy = (shootenemyArray[counter].sprite.getPosition().y + 64) - (player.sprite.getPosition().y + 32);
							Rotation = (atan2(dy, dx)) * 180 / PI;
							if (Rotation < 0)
							{
								enemyprojectile.fire(shootenemyArray[counter].sprite.getPosition().y + 96, shootenemyArray[counter].sprite.getPosition().x + 64, player.sprite.getPosition().y, player.sprite.getPosition().x);
								enemyprojectile.sprite.setOrigin(12, 12);
								enemyprojectile.rect.setOrigin(enemyprojectile.rect.getSize().x / 2, enemyprojectile.rect.getSize().y / 2);
								enemyprojectile.rect.setRotation(Rotation + 90);
								enemyprojectile.sprite.setRotation(Rotation + 90);
								enemyprojectileArray.push_back(enemyprojectile);
								EnemyProjectileSpawnCounter++;
							}
						}
						
					}		
					counter--;
				}
			}
			

			///////////Beamer Attacking////////////
			{
				
				counter = BeamerSpawnCounter;
				for (itBeamer = beamerEnemyArray.end(); itBeamer != beamerEnemyArray.begin(); itBeamer--)
				{
					if (beamerEnemyArray[counter].CheckHit())
					{
						if (beamerEnemyArray[counter].CheckFire())
						{
							const float PI = 3.14159265;
							dx = (beamerEnemyArray[counter].sprite.getPosition().x + 32 * beamerEnemyArray[counter].sprite.getScale().x) - (player.sprite.getPosition().x + 32 + 16);
							dy = (beamerEnemyArray[counter].sprite.getPosition().y + 32 * beamerEnemyArray[counter].sprite.getScale().x) - (player.sprite.getPosition().y + 32 + 21);
							Rotation = (atan2(dy, dx)) * 180 / PI;

							if (Rotation < 0)
							{
								beamProjectile.SetSpeedBoost(SpeedBoostPower - SpeedBoostSlow);
								beamProjectile.SetBeamerPos(beamerEnemyArray[counter].sprite.getPosition().x + 32 * beamerEnemyArray[counter].sprite.getScale().x,
									beamerEnemyArray[counter].sprite.getPosition().y + 32 * beamerEnemyArray[counter].sprite.getScale().x);
								beamProjectile.sprite.setOrigin(16, 0);
								beamProjectile.rect.setOrigin(16, 0);
								beamProjectile.sprite.setRotation(Rotation + 90);
								beamProjectile.rect.setRotation(Rotation + 90);
								beamProjectileArray.push_back(beamProjectile);
								BeamspawnCounter++;
							}
						}
					}
					counter--;
				}
				
			}

			//////////Golem Draw and Update///////////
			{
				counter = GolemSpawnCounter;
				for (itEnemy = enemyArray.end(); itEnemy != enemyArray.begin(); itEnemy--)
				{
					enemyArray[counter].update();
					enemyArray[counter].GetPlayerPos(player.sprite.getPosition().x + 32, player.sprite.getPosition().y + 32);
					if (enemyArray[counter].CheckHit())
					{
						window.draw(enemyArray[counter].sprite);
						if (DrawHitbox)
						{
							window.draw(enemyArray[counter].rect);
						}
					}
					counter--;
				}
			}
			
			//////////Shooter Draw and Update///////////
			{
				counter = ShooterEnemySpawnCounter;
				for (itShooterEnemy = shootenemyArray.end(); itShooterEnemy != shootenemyArray.begin(); itShooterEnemy--)
				{
					shootenemyArray[counter].update();
					if (shootenemyArray[counter].CheckHit())
					{
						window.draw(shootenemyArray[counter].sprite);
						if (DrawHitbox)
						{
							window.draw(shootenemyArray[counter].rect);
						}
					}
					counter--;
				}
			}
			
			//////////Beamer Draw and Update///////////
			{
				counter = BeamerSpawnCounter;
				for (itBeamer = beamerEnemyArray.end(); itBeamer != beamerEnemyArray.begin(); itBeamer--)
				{
					beamerEnemyArray[counter].update();
					if (beamerEnemyArray[counter].CheckHit())
					{
						window.draw(beamerEnemyArray[counter].sprite);
						if (DrawHitbox)
						{
							window.draw(beamerEnemyArray[counter].rect);
	
						}
					}
					counter--;
				}
			}
			
			/////////Beam Drawing and Update///////////
			{
				counter = BeamspawnCounter;
				for (itBeamProjectile = beamProjectileArray.end(); itBeamProjectile != beamProjectileArray.begin(); itBeamProjectile--)
				{
					
					beamProjectileArray[counter].update();
					if (beamProjectileArray[counter].CheckHit())
					{
						window.draw(beamProjectileArray[counter].sprite);

						if (DrawHitbox)
						{
							window.draw(beamProjectileArray[counter].rect);
						}
					}
					counter--;
				}
			}

			/////////Shooter Projectile draw and update/////////
			{

		
				counter = EnemyProjectileSpawnCounter;
				for (itenemyproj = enemyprojectileArray.end(); itenemyproj != enemyprojectileArray.begin(); itenemyproj--)
			{
				enemyprojectileArray[counter].update();
				if (enemyprojectileArray[counter].CheckHit())
				{
					window.draw(enemyprojectileArray[counter].sprite);
					if (DrawHitbox)
					{
						window.draw(enemyprojectileArray[counter].rect);

					}
				}
				counter--;
			}
			}

			///////////Boss Spear Drawing///////////
			{
				counter = BossSpearSpawnCounter;
				for (itBossSpear = bossSpearArray.end(); itBossSpear != bossSpearArray.begin(); itBossSpear--)
				{
					bossSpearArray[counter].update();

					window.draw(bossSpearArray[counter].sprite);
					if (DrawHitbox)
					{
						window.draw(bossSpearArray[counter].rect);
					}

					counter--;
				}
			}


			///////////Power up drawing/////////////
			{
				counter = proaSpawnCounter;
				for (itPROA = proaArray.end(); itPROA != proaArray.begin(); itPROA--)
				{
					proaArray[counter].update();
					if (proaArray[counter].CheckHit())
					{
						window.draw(proaArray[counter].sprite);
						if (DrawHitbox)
						{
							window.draw(proaArray[counter].rect);
						}
					}
					counter--;
				}

				counter = rfSpawnCounter;
				for (itPRF = prfArray.end(); itPRF != prfArray.begin(); itPRF--)
				{
					prfArray[counter].update();
					if (prfArray[counter].CheckHit())
					{
						window.draw(prfArray[counter].sprite);
						if (DrawHitbox)
						{
							window.draw(prfArray[counter].rect);
						}

					}
					counter--;
				}

				counter = psSpawnCounter;
				for (itPS = psArray.end(); itPS != psArray.begin(); itPS--)
				{
					psArray[counter].update();
					if (psArray[counter].CheckHit())
					{
						window.draw(psArray[counter].sprite);
						if (DrawHitbox)
						{
							window.draw(psArray[counter].rect);
						}

					}
					counter--;
				}

				counter = sbSpawnCounter;
				for (itSB = sbArray.end(); itSB != sbArray.begin(); itSB--)
				{
					sbArray[counter].update();
					if (sbArray[counter].CheckHit())
					{
						window.draw(sbArray[counter].sprite);
						if (DrawHitbox)
						{
							window.draw(sbArray[counter].rect);
						}

					}
					counter--;
				}

			}


			//////////Player Drawing////////////
			{

				window.draw(playerUnderdel.sprite);

				window.draw(player.sprite);
				if (DrawHitbox)
				{
					window.draw(player.rect);
				}


			}
			
			
			
			/////////////Boss Draw and Update/////////////
			{
				if (Activateboss)
				{
					if (earthBoss.CheckHit())
					{
						earthBoss.update();
						window.draw(earthBoss.sprite);
						if (DrawHitbox)
							window.draw(earthBoss.rect);
					}
				}
				
				
			}


			///////////Boss Projectile drawing///////////
			{
				counter = BossProjSpawnCounter;
				for (itBossProj = bossprojArray.end(); itBossProj != bossprojArray.begin(); itBossProj--)
				{
					bossprojArray[counter].update();
					if (bossprojArray[counter].CheckHit())
					{
						window.draw(bossprojArray[counter].sprite);
						if (DrawHitbox)
						{
							window.draw(bossprojArray[counter].rect);
						}
					}
					counter--;
				}


				
			}

			if (DrawHitbox)
			{
				window.draw(Leftwall);
				window.draw(Rightwall);
			}
			///////////////////Background Sides Drawing//////////////////
			{
				if (!Activateboss)
				{
					window.draw(BackgroundSidesSpriteX0Y0);
					window.draw(BackgroundSidesSpriteX0Y1);
					window.draw(BackgroundSidesSpriteX0Y2);
					window.draw(BackgroundSidesSpriteX0Y3);
					window.draw(BackgroundSidesSpriteX0Y4);

					window.draw(BackgroundSidesSpriteX1Y0);
					window.draw(BackgroundSidesSpriteX1Y1);
					window.draw(BackgroundSidesSpriteX1Y2);
					window.draw(BackgroundSidesSpriteX1Y3);
					window.draw(BackgroundSidesSpriteX1Y4);
				}
				
				

			}


			///////////Boss Crushing hands drawing////////////
			{
				counter = BossCrushspawnCounter;
				for (itBossCrush = bossCrushArray.end(); itBossCrush != bossCrushArray.begin(); itBossCrush--)
				{
					bossCrushArray[counter].update();

					window.draw(bossCrushArray[counter].sprite);

					if (DrawHitbox)
					{
						window.draw(bossCrushArray[counter].rect);
					}
					counter--;
				}

			}

			////////////////HUD drawing stuff////////////////////////////
			{
				PowerupHUDSprite.setScale(2, 2);
				PowerupHUDSprite.setPosition(SCREENWIDTH - PowerupHUDTexture.getSize().x * PowerupHUDSprite.getScale().x, SCREENHEIGHT - PowerupHUDTexture.getSize().y * PowerupHUDSprite.getScale().y);
				window.draw(PowerupHUDSprite);
				window.draw(hudFace.sprite);
				window.draw(hourglass.sprite);
				window.draw(HUDPowerupSprite);
			}

			////////////////Score drawing & Calculating//////////////////
			{
				counter = 0;
				scoreCalc = Score;
				for (int i = scoreCalc; i >= 10000; i -= 10000)
				{
					counter++;
					scoreCalc -= 10000;
				}
				Score1.setTextureRect(sf::IntRect(counter * 16, 0, 16, 16));
				counter = 0;
				for (int i = scoreCalc; i >= 1000; i -= 1000)
				{
					counter++;
					scoreCalc -= 1000;
				}
				Score2.setTextureRect(sf::IntRect(counter * 16, 0, 16, 16));
				counter = 0;
				for (int i = scoreCalc; i >= 100; i -= 100)
				{
					counter++;
					scoreCalc -= 100;
				}
				Score3.setTextureRect(sf::IntRect(counter * 16, 0, 16, 16));
				counter = 0;
				for (int i = scoreCalc; i >= 10; i -= 10)
				{
					counter++;
					scoreCalc -= 10;
				}
				Score4.setTextureRect(sf::IntRect(counter * 16, 0, 16, 16));
				counter = 0;
				for (int i = scoreCalc; i >= 1; i -= 1)
				{
					counter++;
					scoreCalc -= 1;
				}
				Score5.setTextureRect(sf::IntRect(counter * 16, 0, 16, 16));

				Score1.setPosition(69, SCREENHEIGHT - 24);
				Score2.setPosition(85, SCREENHEIGHT - 24);
				Score3.setPosition(101, SCREENHEIGHT - 24);
				Score4.setPosition(117, SCREENHEIGHT - 24);
				Score5.setPosition(133, SCREENHEIGHT - 24);
				
				window.draw(Score1);
				window.draw(Score2);
				window.draw(Score3);
				window.draw(Score4);
				window.draw(Score5);
			}

			///////////////ERASE STUFF, FUCK EVERYTHING HUHAEUAHEUHEAUHEUAHEUHEAUHEUAHE///////////////
			{
				counter = 0;
				for (itEnemy = enemyArray.begin(); itEnemy != enemyArray.end(); itEnemy++)
				{
					if (enemyArray[counter].Erase == true)
					{
						enemyArray.erase(itEnemy);
						GolemSpawnCounter--;
						break;
					}
					counter++;
				}

				counter = 0;
				for (itShooterEnemy = shootenemyArray.begin(); itShooterEnemy != shootenemyArray.end(); itShooterEnemy++)
				{
					if (shootenemyArray[counter].Erase == true)
					{
						shootenemyArray.erase(itShooterEnemy);
						ShooterEnemySpawnCounter--;
						break;
					}
					counter++;
				}

				counter = 0;
				for (itenemyproj = enemyprojectileArray.begin(); itenemyproj != enemyprojectileArray.end(); itenemyproj++)
				{
					if (enemyprojectileArray[counter].Erase == true)
					{
						enemyprojectileArray.erase(itenemyproj);
						EnemyProjectileSpawnCounter--;
						break;
					}
					counter++;
				}

				counter = 0;
				for (itPROA = proaArray.begin(); itPROA != proaArray.end(); itPROA++)
				{
					if (proaArray[counter].Erase == true)
					{
						proaArray.erase(itPROA);
						proaSpawnCounter--;
						break;
					}
					counter++;
				}

				counter = 0;
				for (itPRF = prfArray.begin(); itPRF != prfArray.end(); itPRF++)
				{
					if (prfArray[counter].Erase == true)
					{
						prfArray.erase(itPRF);
						rfSpawnCounter--;
						break;
					}
					counter++;
				}

				counter = 0;
				for (itPS = psArray.begin(); itPS != psArray.end(); itPS++)
				{
					if (psArray[counter].Erase == true)
					{
						psArray.erase(itPS);
						psSpawnCounter--;
						break;
					}
					counter--;
				}

				counter = 0;
				for (itSB = sbArray.begin(); itSB != sbArray.end(); itSB++)
				{
					if (sbArray[counter].Erase == true)
					{
						sbArray.erase(itSB);
						sbSpawnCounter--;
						break;
					}
					counter++;
				}
				counter = 0;
				for (itMud = mudArray.begin(); itMud != mudArray.end(); itMud++)
				{
					if (mudArray[counter].Erase == true)
					{
						mudArray.erase(itMud);
						MudSpawnCounter--;
						break;
					}
					counter++;
				}
				counter = 0;
				for (itBeamer = beamerEnemyArray.begin(); itBeamer != beamerEnemyArray.end(); itBeamer++)
				{
					if (beamerEnemyArray[counter].Erase == true)
					{
						beamerEnemyArray.erase(itBeamer);
						BeamerSpawnCounter--;
						break;
						
					}
					counter++;
				}

				counter = 0;
				for (itBeamProjectile = beamProjectileArray.begin(); itBeamProjectile != beamProjectileArray.end(); itBeamProjectile++)
				{
					if (beamProjectileArray[counter].Erase == true)
					{
						beamProjectileArray.erase(itBeamProjectile);
						BeamspawnCounter--;
						break;
					}
					counter++;
				}

				counter = 0;
				for (itArrow = arrowArray.begin(); itArrow != arrowArray.end(); itArrow++)
				{

					if (arrowArray[counter].Erase == true)
					{
						
						arrowArray.erase(itArrow);
						
						ArrowSpawnCounter--;
						break;
					}

					counter++;
				}

				counter = 0;
				for (itBossCrush = bossCrushArray.begin(); itBossCrush != bossCrushArray.end(); itBossCrush++)
				{
					if (bossCrushArray[counter].Erase == true)
					{
						bossCrushArray.erase(itBossCrush);
						BossCrushspawnCounter--;
						break;
					}
					counter++;
				}

				counter = 0;
				for (itBossSpear = bossSpearArray.begin(); itBossSpear != bossSpearArray.end(); itBossSpear++)
				{
					if (bossSpearArray[counter].Erase == true)
					{
						bossSpearArray.erase(itBossSpear);
						BossSpearSpawnCounter--;
						break;
					}
					counter++;
				}
				
				counter = 0;
				for (itBossProj = bossprojArray.begin(); itBossProj != bossprojArray.end(); itBossProj++)
				{
					if (bossprojArray[counter].Erase == true)
					{
						bossprojArray.erase(itBossProj);
						BossProjSpawnCounter--;
						break;
					}
					counter++;
				}
			}


			

			window.draw(CrosshairSprite);


			if (Activateboss)
			{
				
				if (FadeoutCounter != 0)
				{
					BlackoutScreen.a = FadeoutCounter;
					BlackoutSquare.setFillColor(BlackoutScreen);
					window.draw(BlackoutSquare);
					FadeoutCounter -= 2.5;
					player.PushPlayertoTop();
					playerUnderdel.Bossfightmode = true;
				}
				
			}
			else if (PreBossFight)
			{
				player.PushPlayertoTop();
				if (player.sprite.getPosition().y <= -100)
				{
					

					BlackoutScreen.a = FadeoutCounter;
					BlackoutSquare.setFillColor(BlackoutScreen);
					window.draw(BlackoutSquare);
					FadeoutCounter += 2.5;

					if (FadeoutCounter == 255)
					{
						
						ClearArraysOnce = true;
						
						Activateboss = true;
					}
						
				}
			}


			window.display();
		}
		
	}

	return 0;
}


void HandleEvents()
{
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		playerFire = true;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
	{
		enemyFire = true;
	}
};

