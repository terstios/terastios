@2016-02-26 Started to fix with the map, after a long time of trying to find a tutorial on tiled, none of them worked so we decided to do an array of numbers instead.

@2016-02-27 Fixed the scrolling background, hitboxes, collision with player and enemy projectiles, and overall merging the codes for the beta playtesting.

@2016-03-01 Started with the classes of the different power ups

@2016-03-02 Finished the power ups all of them are working as intended. Also did some changes to the code to make it smarter.

@2016-03-03 Created the mud that would slow down the player, trickier than expected

@2016-03-04 Did some touch ups in the code for the shooter enemies also made the power ups drop from enemies.

@2016-03-06 Fixed the last things for the beta presentation (Boss, beamer and spawing also a few touch ups.)

@2016-03-09 NO MORE MEMORY LEAKS... (hopefully)