@ 26/2 

Tried to make our new shooterenemies  and make them shoot. having trouble with making the projectiles spawn at the shooterenemy location could not figure it out. 
Even though we put the same values as for our shooting enemies, the projectiles did not spawn accordingly. I tried to rebuild the project and start over but it did not work.


@ 27/2 

When my collegue looked at the problem he noticed that the order of my set positioning x value and y value in main did not match the values in the enemyprojectile class.
It was the same values but in different order. so i swapped places and it worked. 

We also merged our codes with eachother, Which has been troublesome in the past. 



@1/3
Implemented soundeffects and music in the game, Most of the sound is self made by me using the program audacity. I recorded different groupmembers and just mixed with the effects so they sound reasonable.
The musical theme is made by a person outside the group. All sounds and music is wav files. I have only put the SFX in main but we are going to make soundmanager when we are finetuning all the code at the end.
 We just want it to work now before the beta is due.

@ 2/3

Started working on a new enemy we call beamer which we is suppose to shoot just like our shooting enemy. the projectiles however is going to be more powerful and will not shoot as frequently as the other enemyprojectile. 
It all went pretty well but were not entirely happy with how the projectiles behave yet. They are too similar to the other projectiles and we tried to make the projectiles "save" the position of the player and then shoot with a delay.
For the time being only one of our new enemies behave the way we want to the rest is just shooting without delay. It is a work in progress.

@ 5/3 

Fixed Animations on the beamer and tried to make a hitbox at the bottom and the top of the screen. This did not work the way I wanted to because the player kept being pushed away a longer distance than intended.

@ 9/3

Kept Working on the soundeffects, which i found is pretty easy to implement. The hard work is to find the "right" sound that we want to use. I made a "pitchmanager" which manages the pitch of every sound.
The pitchmanager randomizes the pitch of all the soundeffects so that it changes a little everytime it being used. For example every time you shoot an arrow the sound of the arrow will play but with different pitches
so it feels more dynamic.

@ 10/3

Started implementing sounds of the powerups. The speedboost and powershot, also fixed the death animation on the shooting enemy that did not work while showing our beta presentation.
Having some trouble with the timing of the powershot sound, It plays when I use the powerup not when the actual powershot is fired. 